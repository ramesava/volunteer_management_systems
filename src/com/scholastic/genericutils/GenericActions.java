package com.scholastic.genericutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class GenericActions {
	public static void acceptAlert(WebDriver driver)
	{
		Alert alt=driver.switchTo().alert();
		alt.accept();
				
	}
	public static void dismissAlert(WebDriver driver)
	{
		Alert alt=driver.switchTo().alert();
		alt.dismiss();
				
	}
	public static String getAlertText(WebDriver driver)
	{
		Alert alt=driver.switchTo().alert();
		return alt.getText();
				
	}
	public static void mouseover(WebDriver driver,WebElement element)
	{
		Actions act = new Actions (driver);
		act.moveToElement(element).perform();
		
	}
	
	public static void doubleclick(WebDriver driver,WebElement element)
    {
            Actions act = new Actions(driver);
            act.moveToElement(element).doubleClick().perform();
    }
    public static void rightclick(WebDriver driver,WebElement element)
    {
            Actions act = new Actions(driver);
            act.contextClick(element).perform();
    }
    
    public  static void singleselectdropdown(WebElement element, int i)
    {
    
             Select sct = new Select(element);
             sct.selectByIndex(i);
    }
    
 
	
	public static String getExcelData(String sheetName, int row_index, int cell_index)
	{
		String data= null;
		try {
			FileInputStream file_in = new FileInputStream(".\\ExcelLibs\\TestData.xlsx");
			Workbook wb = WorkbookFactory.create(file_in);
			Sheet st = wb.getSheet(sheetName);
			Row r = st.getRow(row_index);
			Cell c = r.getCell(cell_index);
			data = c.getStringCellValue();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
	
	
	public static void setExcelData(String sheetName, int row_index, int cell_index, String data)
	{
		try {
			FileInputStream file_in = new FileInputStream(".\\ExcelLibs\\TestData.xlsx");
			Workbook wb = WorkbookFactory.create(file_in);
			Sheet st = wb.getSheet(sheetName);
			Row r = st.getRow(row_index);
			Cell c = r.getCell(cell_index);
			c.setCellType(Cell.CELL_TYPE_STRING);
			c.setCellValue(data);
			FileOutputStream file_out = new FileOutputStream(".\\ExcelLibs\\TestData.xlsx");
			wb.write(file_out);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void capturescreenshots(WebDriver driver,String Screenshotname) throws IOException
	
	{
		TakesScreenshot ts =(TakesScreenshot)driver;
		
		File source =ts.getScreenshotAs(OutputType.FILE);
		
		FileUtils.copyFile(source, new File("./Screenshots/"+Screenshotname+".png"));
		
		System.out.println("Screenshot taken");
		
	}
		
		
		
	
	
		
				
	}
	

    





