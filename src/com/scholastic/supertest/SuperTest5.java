package com.scholastic.supertest;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeClass;

import com.scholastic.genericutils.GenericActions;
import com.scholastic.logger.AutomationLogger;

public class SuperTest5 {

	public WebDriver driver;
	public Logger log;
	
	
	@BeforeClass
	public void preCondition()
	{
		String browsername = GenericActions.getExcelData("Sheet1", 6, 0);
		String url = GenericActions.getExcelData("Sheet1", 6, 1);
		if(browsername.equals("FF"))
		{
			driver = new FirefoxDriver();
	
		}
		else if(browsername.equals("GC"))
		{
			System.setProperty("webdriver.chrome.driver", ".\\Browser\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		else if(browsername.equals("IE"))
		{
			/*System.setProperty("webdriver.ie.driver", ".\\Browser\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();*/
			
			DesiredCapabilities ieCapabilities=DesiredCapabilities.internetExplorer();
			ieCapabilities.setJavascriptEnabled(true);
			ieCapabilities.setBrowserName("internet explorer");
			ieCapabilities.setPlatform(Platform.ANY);
			
			driver=new RemoteWebDriver(ieCapabilities);
			
		}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.get(url);
	}
	//@AfterClass
	public void postCondition()
	{
		driver.close();
	}
	
	
	public void writeLog(String msg) {
		AutomationLogger.getLogger().info(msg);
	}
	


}


