package com.scholastic.regression;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.scholastic.genericutils.GenericActions;
import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.AdminToolLoginpage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest4;

public class LoginTC001 extends SuperTest4{
	@Test
	 public void logintc001() throws InterruptedException, IOException
	 {
		
		AdminToolLoginpage l= new AdminToolLoginpage(driver);
		Thread.sleep(2000);
		
		AutomationLogger.getLogger().info("AdminTool Login Page displayed");
		GenericActions.capturescreenshots(driver, "AdminToolLoginpageScreenshot");
		l.login1();
		Thread.sleep(2000);
	    
		
		AutomationLogger.getLogger().info("Login unsuccessful with blankusername and password");
		GenericActions.capturescreenshots(driver, "Screenshot for blankusername and blankpassword");
		
		
		l.login();
		Thread.sleep(3000);
		driver.navigate().refresh();
		GenericActions.capturescreenshots(driver, "AdminToolHomepageScreenshot");
		
		AutomationLogger.getLogger().info("Admin Tool Home Page displayed on successful login");
		
		driver.navigate().back();
		
		Thread.sleep(3000);
		
		
		
		l.login2();
		
		AutomationLogger.getLogger().info("Login unsuccessful with invalid username and invalid password");
		
		Thread.sleep(3000);
		
		
		l.login3();
	
		
		
		Thread.sleep(3000);
		
		
		l.login4();
		
		
		
		Thread.sleep(3000);
		
		l.login5();
		
		
		
		Thread.sleep(3000);
		
		l.login6();
	
		
		
		Thread.sleep(3000);
		AutomationLogger.getLogger().info("All the positive and negative scenarios passed");
		
		
		
		
		
	 }
	
	

}