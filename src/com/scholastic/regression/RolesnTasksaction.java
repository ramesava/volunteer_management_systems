package com.scholastic.regression;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import java.io.IOException;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.scholastic.genericutils.GenericActions;
import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.AdminToolHomepage;
import com.scholastic.pagerepo.AdminToolLoginpage;
import com.scholastic.pagerepo.RolesnTasks;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest4;

public class RolesnTasksaction extends SuperTest4{
	@Test
	public void performactionsRolesTaskspage() throws InterruptedException, IOException
	{
		AdminToolLoginpage l= new AdminToolLoginpage(driver);
		Thread.sleep(3000);
		AutomationLogger.getLogger().info("Running  basic tests on Roles and tasks page ");
	
		try {
			GenericActions.capturescreenshots(driver, "AdminToolLoginpage");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		l.login();
		Thread.sleep(3000);
		
		AdminToolHomepage a= new AdminToolHomepage(driver);
		
		try {
			GenericActions.capturescreenshots(driver,"AdminToolHomepage");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread.sleep(3000);
		
		
		a.clicks();
		RolesnTasks r = new RolesnTasks(driver);
		Thread.sleep(3000);
		
		GenericActions.capturescreenshots(driver, "RolesnTaskspage");
		
		
		r.notitle();
		Thread.sleep(3000);
		r.addroles();
		Thread.sleep(5000);
		
		driver.navigate().refresh();
		Thread.sleep(3000);
		//GenericActions.capturescreenshots(driver, "Newly added roles and tasks");
		r.cancel();
		Thread.sleep(3000);
		
		
		
		r.activatedeactivateroles();
		
		Thread.sleep(3000);
		
		r.clickingbuttons();
		
		
		
		
		
		
	}

}
