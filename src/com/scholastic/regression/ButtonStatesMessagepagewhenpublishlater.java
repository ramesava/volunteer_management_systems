package com.scholastic.regression;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import com.scholastic.pagerepo.AdminToolHomepage;
import com.scholastic.pagerepo.AdminToolLoginpage;
import com.scholastic.pagerepo.UpdateMessagePage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest4;

public class ButtonStatesMessagepagewhenpublishlater extends SuperTest4 {
	@Test
	public void buttonstatePublishNow() throws InterruptedException
	{
		AdminToolLoginpage l= new AdminToolLoginpage(driver);
		Thread.sleep(3000);
		l.login();
		Thread.sleep(3000);
		AdminToolHomepage a= new AdminToolHomepage(driver);
		Thread.sleep(3000);
		a.clicks1();
		UpdateMessagePage u =new UpdateMessagePage(driver);
		Thread.sleep(4000);
		u.buttonstateatpublishlater();
	}

}
