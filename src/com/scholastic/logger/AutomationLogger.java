package com.scholastic.logger;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * Makes logger, and associated settings.
 * 
 * @author Prasanjit Choudhury
 *
 */
public class AutomationLogger {
	static Logger logger =null;
	
	public AutomationLogger() {
		String path=System.getProperty("user.dir");
		 logger =Logger.getLogger("AutomationLogger");
		 PropertyConfigurator.configure(path+"\\src\\Log4j.properties");
	}
	
	public static Logger getLogger() {
		new AutomationLogger();
		return logger;
	}
	

}
