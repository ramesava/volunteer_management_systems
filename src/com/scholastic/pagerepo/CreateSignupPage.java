package com.scholastic.pagerepo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.genericutils.GenericActions;
import com.scholastic.logger.AutomationLogger;

public class CreateSignupPage {
	
	private static final String JavascriptExecutor = null;

	public WebDriver driver;
	
	    @FindBy(xpath="//textarea[@ng-model='fair.description']")
		private WebElement Descriptionbox;
	    @FindBy(xpath="//input[@ng-checked='fair.volunteerSignAlert']")
		private WebElement VolunteerSignUpcheckbox;
	    @FindBy(xpath="//input[@ng-checked='fair.volunteerCancelAlert']")
	   	private WebElement VolunteerCancelCheckbox;
	    @FindBy(xpath="html/body/div[2]/div/div/section[2]/div[2]/div/table[2]/tbody/tr[1]/td[6]")
	    private WebElement Spanbox110910am;
	    @FindBy(xpath="(//td[6]/button)[1]")
	   	private WebElement Addbutton110910am;
	    
	    @FindBy(xpath="html/body/div[2]/div/div/section[2]/div/div/table[2]/tbody/tr[1]/td[9]")
	    private WebElement Spanbox11091pm;
	    @FindBy(xpath="(//td[9]/button)[1]")
	   	private WebElement Addbutton11091pm;
	    @FindBy(xpath="//button[@ng-click='cancel()']")
	   	private WebElement Cancel;
	    @FindBy(xpath="//button[@ng-click='saveFair(fair)']")
	   	private WebElement SaveLater;
	    @FindBy(xpath="//button[@ng-click='publishNow(fair.signUpSheetId)']")
	   	private WebElement PublishNow;
	    @FindBy(xpath="//button[@ng-click='closePopUp()']")
	   	private WebElement PublishNowClosePopup;
	    @FindBy (xpath="//button[@data-slide-to='1']")
		private WebElement HelpNext1;
	    @FindBy (xpath="//button[@data-slide-to='2']")
		private WebElement HelpNext2;
	    @FindBy (xpath="//button[@data-slide-to='3']")
		private WebElement HelpNext3;
	    @FindBy (xpath="(//a[@ng-click='CloseCarousel()'])[3]")
	    private WebElement HelpClose;
	    @FindBy (xpath="//button[@ng-click='loadSheetRedirect(orgId, fair.fairId,fair.signUpSheetId)']")
	    private WebElement ContinueCreation;
	    
	    
	    @FindBy(xpath="(//li[@class='bg-success ng-binding ng-scope spanBlock'])[1]")
	    private WebElement RolesVolunteername;
	    
	    public CreateSignupPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
	    
	    
	    public void signingup() throws InterruptedException
	    {
	    	
	    	
	    	//VolunteerSignUpcheckbox.click();
	    	//Thread.sleep(1000);
	    	Descriptionbox.sendKeys("This is a test description");
	    	AutomationLogger.getLogger().info("Test description successfully added for the test fair.TestCase Pass");
	    	GenericActions.mouseover(driver, Spanbox110910am);
	    	Thread.sleep(2000);
	    	Addbutton110910am.click();
	    	Thread.sleep(2000);
	    	
	    	String url = driver.getCurrentUrl();
	    	Thread.sleep(2000);
	    	if (url.equals("https://vms-qa.scholastic.com/#/volunteers/taskDetails"))
	    	{
	    		System.out.println("Clicking on Add button in the calendar page navigates user to Task Details page.Test Case pass ");
	    	}
	    	
	    	else
	    	
	    	{
	    		System.out.println("Clicking on Add button in the calendar page   does not navigate user to Task Details page.Test Case fail");
	    	}
	    	
	    	
	    }

	  public void clicksavelater() throws InterruptedException
	  {
		 
			 SaveLater.click();
			 Thread.sleep(3000);
			String url=driver.getCurrentUrl();
			if (url.equals("https://vms-qa.scholastic.com/#/volunteers/503/372"))
			{
				System.out.println("Clicking on Save for later button in the calendar page navigates user back to the home page of the signupsheet.TestCase Pass");
			}
			else
			{
				System.out.println("Clicking on Save for later button  in the calendar page does not navigate user back to the home page of the signupsheet.Test Case Fail");
			}
			Thread.sleep(3000);
		boolean status=	ContinueCreation.isDisplayed();
		if(status==true)
			{
			System.out.println("Clicking on Save for later button in the calendar page, displays Continue Creation button in home page.Test Case Pass");	
			
			}
		
		else
		{
			System.out.println("Clicking on Save for later button in the calendar page, does not display Continue Creation button in home page.Test Case Fail");
		}
	  }
	  
	  public void clickpublishnow() throws InterruptedException
	  {
		  PublishNow.click();
		  Thread.sleep(1000);
		  PublishNow.click();
		  Thread.sleep(1000);
		  PublishNowClosePopup.click();
		 
	  }
      
	  public void clickcancel()
	  {
		  Cancel.click();
	  }
	  
	  public void safeJavaScriptClick(WebElement element) throws Exception {
			try {
				if (element.isEnabled() && element.isDisplayed()) {
					System.out.println("Clicking on element with using java script click");

					((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
				} else {
					System.out.println("Unable to click on element");
				}
			} catch (StaleElementReferenceException e) {
				System.out.println("Element is not attached to the page document "+ e.getStackTrace());
			} catch (NoSuchElementException e) {
				System.out.println("Element was not found in DOM "+ e.getStackTrace());
			} catch (Exception e) {
				System.out.println("Unable to click on element "+ e.getStackTrace());
			}
		}

	  public void helpoverlay() throws InterruptedException
		 {
			 HelpNext1.click();
			 Thread.sleep(2000);
			 HelpNext2.click();
			 Thread.sleep(2000);
			 HelpClose.click();
			 AutomationLogger.getLogger().info("Help Overlay in the Signup page successfully handled and closed.TestCase Pass");
			 
		 }


      public void continuecreation() throws InterruptedException
      {
    	  ContinueCreation.click();
    	  Thread.sleep(3000);
    	  String url = driver.getCurrentUrl();
    	  Thread.sleep(3000);
    	  if(url.equals("https://vms-qa.scholastic.com/#/volunteers/schedule/501/369"))
    	  {
    		  System.out.println("Clicking on Continue Creation button navigates user to  calendar   page.TestCase Pass");
    		  
    	  }
    	  
    	  else
    	  {
    		  
    		  System.out.println("Clicking on Continue Creation button navigates user to  calendar   page.TestCase Fail");
    		  
    	  }
    	  
    	  
    		  
    		  
    	  
      }



}



