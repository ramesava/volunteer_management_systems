package com.scholastic.pagerepo;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.logger.AutomationLogger;

public class RolesnTasks {
	
	public WebDriver driver;
	@FindBy(xpath="//input[@name='title']")
	private WebElement TitleTextbox;
	@FindBy(xpath="//textarea[@name='description']")
	private WebElement Descriptionbox;
	@FindBy(xpath="//input[@id='fakeBrowse']")
	private WebElement BrowseButton1;
	@FindBy(xpath="//input[@id='fakeBrowse1']")
	private WebElement BrowseButton2;
	@FindBy(xpath="//button[contains(text(), 'Inactivate')]")
	private WebElement InactivateButton;
	@FindBy(xpath="//button[contains(text(), 'Activate')]")
	private WebElement ActivateButton;
	@FindBy(xpath="//button[contains(text(), 'Save')]")
	private WebElement SaveButton;
	@FindBy(xpath="//button[contains(text(), 'Save & Add Another')]")
	private WebElement SavenAddButton;
	@FindBy(xpath="//button[contains(text(), 'Publish Later')]")
	private WebElement Publishlater;
	@FindBy(xpath="//button[contains(text(), 'Publish Now')]")
	private WebElement PublishNowButton;
	@FindBy(xpath="//button[contains(text(), 'Cancel')]")
	private WebElement CancelButton;
	@FindBy (xpath="//a[contains(text(), 'Go Back')]")
	private WebElement Goback;
	@FindBy (xpath="//a[contains(text(), 'Teacher51')]")
	private WebElement Teacher51;
	@FindBy (xpath="//a[contains(text(), 'Teacher52')]")
	private WebElement Teacher52;
	@FindBy (partialLinkText="Teacher53")
	private WebElement Teacher53;
	@FindBy (partialLinkText="Teacher54")
	private WebElement Teacher54;
	@FindBy (partialLinkText="Teacher55")
	private WebElement Teacher55;
	@FindBy (partialLinkText="Teacher69")
	private WebElement Teacher69;
	@FindBy (partialLinkText="Lakhan12345")
	private WebElement Lakhan12345;
	
	@FindBy (xpath="//span[contains(text(), 'Title')]")
	private WebElement Titlecantbeblank;
	
	
	
	
	public RolesnTasks (WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

   public void addroles() throws InterruptedException, IOException
   { 
	   TitleTextbox.sendKeys("Example2");
	   Thread.sleep(2000);
	   Descriptionbox.sendKeys("This is only for Test purpose");
	   Thread.sleep(2000);
	   BrowseButton1.click();
	   Thread.sleep(2000);
	   Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
	   Thread.sleep(2000);
	  //BrowseButton1.click();
	 //Thread.sleep(4000);
	// Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload1.exe");
	//Thread.sleep(2000);
		  
	BrowseButton2.click();
    Thread.sleep(2000);
			 
   Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
	 SaveButton.click();
	 Thread.sleep(2000);
	 
	 WebElement ele =driver.findElement(By.xpath("//a[contains(text(),'Example2')]"));
	 if (ele.isDisplayed()){
		 System.out.println("Roles added successfully on click of Save button.TestCase passed");
	 }
	 else
	 {
		 System.out.println("Role not added successfully on click of Save button.Testcase failed");
	 }
	 Thread.sleep(2000);
	 TitleTextbox.sendKeys("Example3");
	 Descriptionbox.sendKeys("This is only for Test purposes");
	 Thread.sleep(3000);
	 SavenAddButton.click();
	 
Thread.sleep(2000);
	 
	 WebElement ele1 =driver.findElement(By.xpath("//a[contains(text(),'Example3')]"));
	 if (ele1.isDisplayed()){
		 System.out.println("Roles added successfully on click of Save and Add  button.TestCase passed");
	 }
	 else
	 {
		 System.out.println("Role not added successfully on click of Save and Add button.Testcase failed");
	 }
	   
   }
   
   public void cancel() throws InterruptedException
   
   {
	   TitleTextbox.sendKeys("Example4");
	   Descriptionbox.sendKeys("This is only for Test purpose");
	   Thread.sleep(2000);
	   CancelButton.click();
	   Thread.sleep(2000);
	   boolean status =driver.findElements(By.xpath("//a[contains(text(),'Example4')]")).size()>0;
	   if (status==true)
	   {
			 System.out.println("Roles  added  on click of Cancel  button.TestCase failed");
		 }
		 else
		 {
			 System.out.println("Role not added  on click of Cancel button.Testcase pass");
			
		 }
	  
	   
	   
   }
   
   public void clickingbuttons() throws InterruptedException
   {
	   
	   Goback.click();
	   Thread.sleep(3000);
	   String url=driver.getCurrentUrl();
	   if (url.equals("https://vms-qa.scholastic.com/#/admin/home"))
	   {
		   System.out.println("Clicking Goback button in the Rolesand Taskspage navigates the user back to the Home Page.Testcase passed");
	   }
	   else
	   {
		   System.out.println("Clicking Goback button in the Rolesand Taskspage does  navigate the user back to the Home Page.Testcase failed");
		   
	   }
	   
		   
	  
	} 
   
   public void activatedeactivateroles() throws InterruptedException
   {
 	  driver.findElement(By.xpath("//a[contains(text(),'Example2')]")).click();
 	  Thread.sleep(3000);
 	  InactivateButton.click();
 	  Thread.sleep(1000);
 	  if(driver.findElement(By.xpath("//button[contains(text(),'Activate')]")).isDisplayed())
 			  {
 		        System.out.println("Clicking on Inactivate button for an active role  displays the Activate button.Test case pass");
 		        
 			  }
 	  
 	  else
 		  
 	  {
 		 System.out.println("Clicking on Inactivate button for an active role   does not  display the Activate button.Test case failed");
 	  }
 	  
 	  
 	  Thread.sleep(1000);
 	  //driver.navigate().refresh();
 	 driver.findElement(By.xpath("//a[contains(text(),'Example2')]")).click(); 
 	  Thread.sleep(2000);
 	  ActivateButton.click();
 	 Thread.sleep(1000);
 	 if(driver.findElement(By.xpath("//button[contains(text(),'Inactivate')]")).isDisplayed())
	  {
        System.out.println("Clicking on Activate button for an inactive role  displays the Inactivate button.Test case pass");
        
	  }

else
  
{
 System.out.println("Clicking on Activate button for an inactive role   does not  display the Inactivate button.Test case failed");
}
 	  
 	  
 	  
 }
   public void buttonstateatfirstlogin()
   {
 	  boolean st= Goback.isEnabled();
 	  if (st == true)
 	  {
       System.out.println("Goback button is enabled at first login.TestCasepassed");
 	  }
 	  else
 	  {
 		  System.out.println("Goback button is disabled at first login.TestCasefailed");
 	  }
 	  boolean st1= CancelButton.isEnabled();
 	  if (st1 == true)
 	  {
       System.out.println("Cancel button is enabled at first login.TestCasepassed");
 	  }
 	  else
 	  {
 		  System.out.println("Cancel button is disabled at first login.TestCasefailed");
 	  }
 	  boolean st2= SaveButton.isEnabled();
 	  if (st2 == true)
 	  {
       System.out.println("Save button is enabled at first login.TestCasepassed");
 	  }
 	  else
 	  {
 		  System.out.println("Save button is disabled at first login.TestCasefailed");
 	  }
 	  boolean st3= SavenAddButton.isEnabled();
 	  if (st3 == true)
 	  {
       System.out.println("SavenAddButton is enabled at first login.TestCasepassed");
 	  }
 	  else
 	  {
 		  System.out.println("SavenAddButton is disabled at first login.TestCasefailed");
 	  }
 	  boolean st4= Publishlater.isEnabled();
 	  if (st4 == true)
 	  {
       System.out.println("Publishlater is enabled at first login.TestCasepassed");
 	  }
 	  else
 	  {
 		  System.out.println("Publishlater is disabled at first login.TestCasefailed");
 	  }
 	  boolean st5= PublishNowButton.isEnabled();
 	  if (st5 == true)
 	  {
       System.out.println("PublishNowButton is enabled at first login.TestCasepassed");
 	  }
 	  else
 	  {
 		  System.out.println("PublishNowButton is disabled at first login.TestCasefailed");
 	  }
 	  
 	  }
 		  
       public void buttonstatesatpublishnow() throws InterruptedException, IOException
       {
     	  TitleTextbox.sendKeys("Teacher54");
    	   Descriptionbox.sendKeys("This is only for Test purpose");
    	   Thread.sleep(2000);
    	   BrowseButton1.click();
    		Thread.sleep(4000);
    		 
    		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
    		  Thread.sleep(2000);
    		  BrowseButton2.click();
    		  Thread.sleep(4000);
    			 
    		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
    		  PublishNowButton.click();
    		  Thread.sleep(3000);
    		  driver.navigate().refresh();
    		  Thread.sleep(3000);
    		  Teacher54.click();
    		  Thread.sleep(2000);
    		boolean st6= PublishNowButton.isDisplayed();
   	    if (st6 == true)
   	  {
         System.out.println("PublishNowButton is displayed even when we have click  a role after doing a  published now for the  role.TestCaseFailed");
   	  }
   	  else
   	  {
   		  System.out.println("PublishNowButton is not  displayed  when we have click a role after doing a published now for  the  role.TestCasepassed");
   	  }
   	    
   	  boolean st7= Publishlater.isDisplayed();
 	    if (st7 == true)
 	  {
       System.out.println("PublishLater is displayed even when we have click  a role after doing a  published  later for the  role.TestCaseFailed");
 	  }
 	  else
 	  {
 		  System.out.println("Publish later Button is not  displayed when we  click a role after doing a  published later for the  role.TestCasepassed");
 	  }
    		  
    	}
       
       public void buttonstateatpublishlater() throws InterruptedException, IOException
       {
     	  TitleTextbox.sendKeys("Teacher55");
 	   	   Descriptionbox.sendKeys("This is only for Test purpose");
 	   	   Thread.sleep(2000);
 	   	   BrowseButton1.click();
 	   		Thread.sleep(4000);
 	   		 
 	   		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
 	   		  Thread.sleep(2000);
 	   		  BrowseButton2.click();
 	   		  Thread.sleep(4000);
 	   			 
 	   		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
 	   		  Publishlater.click();
 	   		  Thread.sleep(3000);
 	   		  driver.navigate().refresh();
 	   		  Thread.sleep(3000);
 	   		   Teacher55.click();
 	   		  Thread.sleep(2000);
 	   		boolean st8= Publishlater.isDisplayed();
 	  	    if (st8 == true)
 	  	  {
 	        System.out.println("Publishlater is displayed even when we click a role after doing a publish later for  the  role.TestCaseFailed");
 	  	  }
 	  	  else
 	  	  {
 	  		  System.out.println("Publishlater is not displayed when we click a  role after doing a publish later for the  role.TestCasepassed");
 	  	  }
 	  	    
 	  	  boolean st9= PublishNowButton.isDisplayed();
 		    if (st9 == true)
 		  {
 	      System.out.println("Publishnow button is displayed even when we click a  role after doing a publish later for  the  role.TestCasepassed");
 		  }
 		  else
 		  {
 			  System.out.println("Publish now Button is  not displayed when we click a  role after doing  a publish later for  the  role.TestCasefail");
 		  }	 
       }
           public void buttonstateatSave() throws InterruptedException, IOException
           {
         	  TitleTextbox.sendKeys("Teacher52");
           	   Descriptionbox.sendKeys("This is only for Test purpose");
           	   Thread.sleep(2000);
           	   BrowseButton1.click();
           		Thread.sleep(4000);
           		 
           		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
           		  Thread.sleep(2000);
           		  BrowseButton2.click();
           		  Thread.sleep(4000);
           			 
           		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
           		  SaveButton.click();
           		  Thread.sleep(3000);
           		  driver.navigate().refresh();
           		  Thread.sleep(3000);
           		  Teacher52.click();
           		  Thread.sleep(2000);
           		boolean st10= SaveButton.isEnabled();
          	    if (st10 == true)
          	  {
                System.out.println("SaveButton is disabled or not displayed  even when we have clicked Save after adding the  role.TestCasepass");
          	  }
          	  else
          	  {
          		  System.out.println("SaveButton is enabled or displayed  when we have clicked Save after adding the  role.TestCasefail");
          	  }
                  boolean st11= PublishNowButton.isEnabled();
        	    if (st11 == true)
        	  {
              System.out.println("Publishnow button is enabled even when we have clicked Save while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Publish now Button is disabled when we have clicked Save while adding the  role.TestCasefail");
        	  }
          	    
          	  



                   boolean st12= Publishlater.isEnabled();
        	    if (st12 == true)
        	  {
              System.out.println("Publishlaterbutton is enabled even when we have clicked Save while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Publishlater Button is disabled when we have clicked Save while adding the  role.TestCasefail");
        	  }

                  boolean st13= SavenAddButton.isEnabled();
        	    if (st13 == true)
        	  {
              System.out.println("Saven and AddButton is enabled even when we have clicked Save while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("SavenAddButton is disabled when we have clicked Save while adding the  role.TestCasefail");
        	  }
                  boolean st14= CancelButton.isEnabled();
        	    if (st14 == true)
        	  {
              System.out.println("CancelButton is enabled even when we have clicked Save while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("CancelButton is disabled when we have clicked Save while adding the  role.TestCasefail");
        	  }
                  boolean st15= Goback.isEnabled();
        	    if (st15 == true)
        	  {
              System.out.println("Goback is enabled even when we have clicked Save while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Goback is disabled when we have clicked Save while adding the  role.TestCasefail");
        	  }
                 
         
           }
           
           public void buttonstateatSaveandAdd() throws InterruptedException, IOException
           {
         	  TitleTextbox.sendKeys("Teacher53");
           	   Descriptionbox.sendKeys("This is only for Test purpose");
           	   Thread.sleep(2000);
           	   BrowseButton1.click();
           		Thread.sleep(4000);
           		 
           		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
           		  Thread.sleep(2000);
           		  BrowseButton2.click();
           		  Thread.sleep(4000);
           			 
           		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
           		  SavenAddButton.click();
           		  Thread.sleep(3000);
           		  driver.navigate().refresh();
           		  Thread.sleep(3000);
           		  Teacher53.click();
           		  Thread.sleep(2000);
           		boolean st16= SavenAddButton.isEnabled();
          	    if (st16 == true)
          	  {
                System.out.println("Save and Add Button is enabled even when we have clicked Save and Add Button while adding the  role.TestCasePass");
          	  }
          	  else
          	  {
          		  System.out.println("Save and Add Button is disabled when we have clicked Save and Add Button while adding the  role.TestCaseFail");
          	  }
                  boolean st17= PublishNowButton.isEnabled();
        	    if (st17 == true)
        	  {
              System.out.println("Publishnow button is enabled even when we have clicked Save and Add Button while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Publish now Button is disabled when we have clicked Save and Add Button while adding the  role.TestCasefail");
        	  }
          	    
          	  



                   boolean st18= Publishlater.isEnabled();
        	    if (st18 == true)
        	  {
              System.out.println("Publishlaterbutton is enabled even when we have clicked Save and Add Button while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Publishlater Button is disabled when we have clicked Save and Add Button while adding the  role.TestCasefail");
        	  }
        	    
        	    Thread.sleep(5000);

                  boolean st19= SaveButton.isDisplayed();
        	    if (st19 == true)
        	  {
              System.out.println("SaveButton is enabled or displayed even when we have clicked Save and Add Button while adding the  role.TestCasefailed");
        	  }
        	  else
        	  {
        		  System.out.println("SaveButton is disabled or not displayed when we have clicked Save and Add Button while adding the  role.TestCasepassed");
        	  }
                  boolean st20= CancelButton.isEnabled();
        	    if (st20 == true)
        	  {
              System.out.println("CancelButton is enabled even when we have clicked Save and Add Button while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("CancelButton is disabled when we have clicked Save and Add Button while adding the  role.TestCasefail");
        	  }
                  boolean st21= Goback.isEnabled();
        	    if (st21 == true)
        	  {
              System.out.println("Goback is enabled even when we have clicked Save and Add Button while adding the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Goback is disabled when we have clicked Save and Add Button while adding the  role.TestCasefail");
        	  }
                 
         
           }

           public void buttonstateatEdit() throws InterruptedException, IOException 
           {
         	  TitleTextbox.sendKeys("Teacher51");
           	   Descriptionbox.sendKeys("This is only for Test purpose");
           	   Thread.sleep(2000);
           	   BrowseButton1.click();
           		Thread.sleep(4000);
           		 
           		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
           		  Thread.sleep(2000);
           		  BrowseButton2.click();
           		  Thread.sleep(4000);
           			 
           		  Runtime.getRuntime().exec("C:\\Users\\rle0270\\Desktop\\AutoIT\\File Upload.exe");
           		  SavenAddButton.click();
           		  Thread.sleep(3000);
           		  driver.navigate().refresh();
           		  Thread.sleep(3000);
           		  Teacher51.click();
                           TitleTextbox.sendKeys("Teacher52");
           		  Thread.sleep(2000);
           		boolean st99= SavenAddButton.isEnabled();
          	    if (st99 == true)
          	  {
                System.out.println("Save and Add Button is enabled when we edit a role.TestCasePass");
          	  }
          	  else
          	  {
          		  System.out.println("Save and Add Button is disabled when we edit a role.TestCaseFail");
          	  }
                  boolean st98= PublishNowButton.isEnabled();
        	    if (st98 == true)
        	  {
              System.out.println("Publishnow button is enabled even when we edit the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Publish now Button is disabled when we edit the  role.TestCasefail");
        	  }
          	    
          	  



                   boolean st97= Publishlater.isEnabled();
        	    if (st97 == true)
        	  {
              System.out.println("Publishlaterbutton is enabled even when we edit the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Publishlater Button is disabled when we edit the  role.TestCasefail");
        	  }

                  boolean st96= SaveButton.isEnabled();
        	    if (st96 == true)
        	  {
              System.out.println("SaveButton is enabled or displayed even when we edit the  role.TestCase pass");
        	  }
        	  else
        	  {
        		  System.out.println("SaveButton is disabled when we edit the  role.TestCasefailed");
        	  }
                  boolean st95= CancelButton.isEnabled();
        	    if (st95 == true)
        	  {
              System.out.println("CancelButton is enabled even when we have edit the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("CancelButton is disabled when we edit the  role.TestCasefail");
        	  }
                  boolean st94= Goback.isEnabled();
        	    if (st94 == true)
        	  {
              System.out.println("Goback is enabled even when edit the  role.TestCasepassed");
        	  }
        	  else
        	  {
        		  System.out.println("Goback is disabled when we edit the  role.TestCasefail");
        	  }
                 
         
           }
           
           public void maxlengthTitle() throws InterruptedException
           {
         	  TitleTextbox.sendKeys("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaert");
         	  Thread.sleep(2000);
         	  String str =TitleTextbox.getAttribute("value");
         	  
         	  System.out.println(str.length());
         	  int Titlecharcount= str.length();
         	  if (Titlecharcount>50)
         	  {
         	  System.out.println("Titlebox takes more than 50 characters.Test Case failed");
         	  }
         	  
         	  else
         	  {
         		  
         		  System.out.println("Titlebox takes maximum of  50 characters.Test Case passed");
         		  
         	  }
           }
           
           
           public void maxlengthDescriptionbox() throws InterruptedException
     	  {
     		  Descriptionbox.sendKeys("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaertaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
     		  Thread.sleep(2000);
         	  String str1 = Descriptionbox.getAttribute("value");
         	  
         	  System.out.println(str1.length());
         	  int Descriptioncharcount= str1.length();
         	  if ( Descriptioncharcount>500)
         	  {
         	  System.out.println("Descriptionbox takes more than 500 characters.Test Case failed");
         	  }
         	  
         	  else
         	  {
         	   System.out.println("Descriptionbox takes maximum of  50 characters.Test Case passed");
         		  
         	  }
           }
           
           public void notitle()
           {
        	   TitleTextbox.sendKeys("");
        	   Descriptionbox.sendKeys("This is only for Test purpose");
        	   SaveButton.click();
        	  boolean st1 = Titlecantbeblank.isDisplayed();
        	  if (st1 == true)
        	  {
        		  System.out.println("Title can't be blank is displayed when user clicks Save button without entering any title.Testcase passed");
        		  
        	 }
        	  
        	  else
        		  
        	  {
        		  System.out.println("Title can't be blank is not displayed when user clicks Save button without entering any title.Testcase failed");
        	  }
        	   
        	   
        	   
        	   
           }
           
           


}
  
  
 