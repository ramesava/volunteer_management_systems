package com.scholastic.pagerepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UpdateMessagePage {

	public WebDriver driver;
	@FindBy(xpath="//textarea[@name='message']")
	private WebElement messageTextbox;
	@FindBy(xpath="//button[contains(text(), 'Save')]")
	private WebElement Savebutton;
	@FindBy(xpath="//button[contains(text(), 'Publish Later')]")
	private WebElement PublishlaterButton;
	@FindBy(xpath="//button[contains(text(), 'Publish Now')]")
	private WebElement PublishNowButton;
	@FindBy(xpath="//button[contains(text(), 'Cancel')]")
	private WebElement CancelButton;
	@FindBy (xpath="//a[contains(text(), 'Go Back')]")
	private WebElement Goback;
    @FindBy (xpath="//span[contains(text(), 'Message')]")
	private WebElement Messagecantbeblank;
	
	
	
	
	public UpdateMessagePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void nomessagetyped()
	{
		messageTextbox.clear();
		   messageTextbox.sendKeys("");
		   Savebutton.click();
		   boolean st89 = Messagecantbeblank.isDisplayed();
     	  if (st89 == true)
     	  {
     		  System.out.println("Message  can't be blank is displayed when user clicks Save button without entering any message.Test case pass");
     		  
     	 }
     	  
     	  else
     		  
     	  {
     		  System.out.println("Message  can't be blank is not displayed when user clicks Save button without entering any message.Test case fail");
     	  }
     	   
	}
	
	
	
	
	
	
	public void typemessage() throws InterruptedException
  
   { 
	   messageTextbox.clear();
	   messageTextbox.sendKeys("QA Admin Message - Updated");
	   Savebutton.click();
	   Thread.sleep(7000);
	   if(driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).isEnabled())
	   {
		   System.out.println("Upon clicking Save button after entering a volunteer message,Save button is not displayed.Testcase pass");
	   }
	   
	   else
	   {
		   System.out.println("Upon clicking Save button after entering a volunteer message,Save button is still displayed.Testcase fail"); 
	   }
	   
	   Thread.sleep(2000);
	   messageTextbox.clear();
	   messageTextbox.sendKeys("QA Admin Message - Updated");
	   
	   Thread.sleep(5000);
	   
	   if(driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).isDisplayed())
	
	   {
		   System.out.println("Upon editing volunteer message,Save button is displayed.Testcase pass");
	   }
	   
	   else
	   {
		   System.out.println("Upon editing volunteer message,Save button is still displayed.Testcase fail"); 
	   }
	   
	   Goback.click();
	   
	  
   }
   
   public void cancelaction() throws InterruptedException
   
   {
	   messageTextbox.clear();
	   messageTextbox.sendKeys("QA Admin Message - Updated");
	   Thread.sleep(1000);
	   CancelButton.click();
	   
	   Thread.sleep(4000);
	   String msg= messageTextbox.getText();
	   
	   Thread.sleep(3000);
	   
	   System.out.println(msg);
	   
	   if(msg.equals(""))
	   {
		System.out.println("Upon clicking Cancel button, the message text box gets cleared.Testcase pass") ;  
	   }
	   
	   else
	   {
		System.out.println("Upon clicking Cancel button, the message text box  does not gets cleared.Testcase fail") ;  
	   }
	   
	   Thread.sleep(2000);
	   
	   Goback.click();
	}
   public void buttonstateatfirstlogin()
   {
	   boolean st= Goback.isEnabled();
		  if (st == true)
		  {
	      System.out.println("Goback button is enabled at first login in the Volunteer Message Page.TestCasepassed");
		  }
		  else
		  {
			  System.out.println("Goback button is disabled at first login in the Volunteer Message Page.TestCasefailed");
		  }
		  boolean st1= CancelButton.isEnabled();
		  if (st1 == true)
		  {
	      System.out.println("Cancel button is enabled at first login in the Volunteer Message Page.TestCasepassed");
		  }
		  else
		  {
			  System.out.println("Cancel button is disabled at first login in the Volunteer Message Page.TestCasefailed");
		  }
		  
		  
		  boolean st3= PublishlaterButton.isEnabled();
		  if (st3 == true)
		  {
	      System.out.println("Publishlater is enabled at first login in the Volunteer Message Page.TestCasepassed");
		  }
		  else
		  {
			  System.out.println("Publishlater is disabled at first login in the Volunteer Message Page.TestCasefailed");
	          }
	          boolean st4= PublishNowButton.isEnabled();
		  if (st4 == true)
		  {
	      System.out.println("PublishNow is enabled at first login in the Volunteer Message Page.TestCasepassed");
		  }
		  else
		  {
			  System.out.println("PublishNow is disabled at first login in the Volunteer Message Page.TestCasefailed");
	          }
   
         String str =messageTextbox.getText();
         boolean st5 = Savebutton.isEnabled();
         if (st5==true)
         {  
        	
        	System.out.println("Save button is enabled when user first navigates to Volunteer Message page .Testcase passed");
         }
         
         else
         {
        	 System.out.println("Save button is disabled when user first navigates to volunteer Message page.Testcase fail");
         }
         }
         
   public void buttonstateatpublishnow() throws InterruptedException
   {
  	 messageTextbox.clear();
	     messageTextbox.sendKeys("TestMessage for Testing Publish Now");
	     PublishNowButton.click();
	     Thread.sleep(2000);  	  
	   boolean st= PublishNowButton.isDisplayed();
	  if (st == true)
	  {
     System.out.println("PublishNowButton is displayed when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasefailed");
	  }
	  else
	  {
		  System.out.println("PublishNowButton is not displayed when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasepass");
	  }
	  boolean st1= PublishlaterButton.isDisplayed();
	  if (st1 == true)
	  {
     System.out.println("PublishlaterButton is displayed when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasefailed");
	  }
	  else
	  {
		  System.out.println("PublishlaterButton is not displayed when we click publishnow after typing  a message  in the Volunteer MessagePage.TestCasepassed");
	  }
	  boolean st2= Savebutton.isEnabled();
	  if (st2 == true)
	  {
     System.out.println("Save button is enabled when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasepassed");
	  }
	  else
	  {
		  System.out.println("Save button is disabled when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasefailed");
	  }
	  
	  boolean st3= CancelButton.isEnabled();
	  if (st3 == true)
	  {
     System.out.println("CancelButton is enabled when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasepassed");
	  }
	  else
	  {
		  System.out.println("CancelButton is disabled when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasefailed");
         }
         boolean st4= Goback.isEnabled();
	  if (st4 == true)
	  {
     System.out.println("Goback button is enabled when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasepassed");
	  }
	  else
	  {
		  System.out.println("Goback is disabled when we click publishnow after typing  a message  in the Volunteer Message Page.TestCasefailed");
         }
	   
   }
   public void buttonstateatpublishlater() throws InterruptedException
   {
  	    messageTextbox.clear();
   	     messageTextbox.sendKeys("TestMessage for Testing Publish Later");
   	     PublishlaterButton.click();
   	     Thread.sleep(2000);  	  
   	   boolean st= PublishlaterButton.isDisplayed();
  	  if (st == true)
  	  {
        System.out.println("PublishlaterButton is displayed when we click publishlater after typing  a message  in the Volunteer Message Page.TestCasefailed");
  	  }
  	  else
  	  {
  		  System.out.println("PublishlaterButton is not displayed when we click publishlater after typing  a message  in the Volunteer Message Page.TestCasepass");
  	  }
  	  boolean st1= PublishNowButton.isEnabled();
  	  if (st1 == true)
  	  {
        System.out.println("PublishNowButton is enabled and displayed when we click publish later  after typing  a message  in the Volunteer Message Page.TestCasePassed");
  	  }
  	  else
  	  {
  		  System.out.println("PublishNow Button is disabled when we click publish later after typing  a message  in the Volunteer MessagePage.TestCasefailed");
  	  }
  	  boolean st2= Savebutton.isEnabled();
  	  if (st2 == true)
  	  {
        System.out.println("Save button is enabled when we click publishlater after typing  a message  in the Volunteer Message Page.TestCasepassed");
  	  }
  	  else
  	  {
  		  System.out.println("Save button is disabled when we click publish later after typing  a message  in the Volunteer Message Page.TestCasefailed");
  	  }
  	  
  	  boolean st3= CancelButton.isEnabled();
  	  if (st3 == true)
  	  {
        System.out.println("CancelButton is enabled when we click publish later after typing  a message  in the Volunteer Message Page.TestCasepassed");
  	  }
  	  else
  	  {
  		  System.out.println("CancelButton is disabled when we click publish later after typing  a message  in the Volunteer Message Page.TestCasefailed");
            }
            boolean st4= Goback.isEnabled();
  	  if (st4 == true)
  	  {
        System.out.println("Goback button is enabled when we click publish later after typing  a message  in the Volunteer Message Page.TestCasepassed");
  	  }
  	  else
  	  {
  		  System.out.println("Goback is disabled when we click publish later after typing  a message  in the Volunteer Message Page.TestCasefailed");
            }
   }
     
     public void buttonstateatsave() throws InterruptedException
     {
    	    messageTextbox.clear();
     	    messageTextbox.sendKeys("TestMessage for Save button");
     	   Savebutton.click();
     	     Thread.sleep(3000);  	  
     	   boolean st78= PublishlaterButton.isEnabled();
    	  if (st78 == true)
    	  {
          System.out.println("PublishlaterButton is enabled and displayed when we click Save after typing  a message  in the Volunteer Message Page.TestCasepassed");
    	  }
    	  else
    	  {
    		  System.out.println("PublishlaterButton is disabled when we click Save after typing  a message  in the Volunteer Message Page.TestCasefail");
    	  }
    	  boolean st77= PublishNowButton.isEnabled();
    	  if (st77 == true)
    	  {
          System.out.println("PublishNowButton is enabled and displayed when we click Save later  after typing  a message  in the Volunteer Message Page.TestCasePassed");
    	  }
    	  else
    	  {
    		  System.out.println("PublishNow Button is disabled when we click Save after typing  a message  in the Volunteer MessagePage.TestCasefailed");
    	  }
    	  boolean st76= Savebutton.isDisplayed();
    	  if (st76 == true)
    	  {
          System.out.println("Save button is displayed when we click Save after typing  a message  in the Volunteer Message Page.TestCasefailed");
    	  }
    	  else
    	  {
    		  System.out.println("Save button is not displayed when we click Save after typing  a message  in the Volunteer Message Page.TestCasepass");
    	  }
    	  
    	  boolean st75= CancelButton.isEnabled();
    	  if (st75 == true)
    	  {
          System.out.println("CancelButton is enabled when we click Save after typing  a message  in the Volunteer Message Page.TestCasepassed");
    	  }
    	  else
    	  {
    		  System.out.println("CancelButton is disabled when we click Save after typing  a message  in the Volunteer Message Page.TestCasefailed");
              }
              boolean st74= Goback.isEnabled();
    	  if (st74 == true)
    	  {
          System.out.println("Goback button is enabled when we click Save after typing  a message  in the Volunteer Message Page.TestCasepassed");
    	  }
    	  else
    	  {
    		  System.out.println("Goback is disabled when we click Save after typing  a message  in the Volunteer Message Page.TestCasefailed");
              }
    	  //Goback.click();
    	  //AdminToolHomepage a= new AdminToolHomepage(driver);
    	  //a.clicks//1();
    	 //.sleep(4000);
    	 // st73= Savebutton.isEnabled();
    	 // if (st73 == true)
    	 // {
         // System.out.println("Save button is enabled when we click Save after typing  a message  in the Volunteer Message Page and then go back and navigate to the message page again.TestCasefailed");
    	 // }
    	//  else
    	  //{
    	//	  System.out.println("Save button is disabled when we click Save after typing  a message  in the Volunteer Message Page and then go back and navigate to the message page again.TestCasepass");
    	 // }
    	  
    	 }
     
     public void nomessage()
     {
  	   messageTextbox.clear();
  	   messageTextbox.sendKeys("");
  	   Savebutton.click();
  	  boolean st89 = Messagecantbeblank.isDisplayed();
  	  if (st89 == true)
  	  {
  		  System.out.println("Message  can't be blank is displayed when user clicks Save button without entering any message.Test case pass");
  		  
  	 }
  	  
  	  else
  		  
  	  {
  		  System.out.println("Message  can't be blank is not displayed when user clicks Save button without entering any message.Test case fail");
  	  }
     } 
     
     public void maxlengthMessagebox() throws InterruptedException
     {
   	  messageTextbox.sendKeys("ahsgdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffsadfsaafffffffdddddfgggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggr");
   	  Thread.sleep(2000);
   	  String str =messageTextbox.getAttribute("value");
   	  
   	  System.out.println(str.length());
   	  int Messagecharcount= str.length();
   	  if (Messagecharcount>500)
   	  {
   	  System.out.println("Messagebox takes more than 500 characters.Test Case failed");
   	  }
   	  
   	 else
	  {
		  
		  System.out.println("Messagebox takes maximum of  500 characters.Test Case passed");
		  
	  }
  }

     
public void buttonstateatedit() throws InterruptedException
{
    messageTextbox.clear();
    messageTextbox.sendKeys("Qa Admin Message Edited");
 	  
 	     Thread.sleep(2000);  	  
 	   boolean st21= PublishlaterButton.isEnabled();
	  if (st21 == true)
	  {
      System.out.println("PublishlaterButton is enabled when we Edit a volunteer message in the Volunteer Message page .TestCasepassed");
	  }
	  else
	  {
		  System.out.println("PublishlaterButton is disabled when we Edit a volunteer message in the Volunteer Message page .TestCasefailed");
	  }
	  boolean st22= PublishNowButton.isEnabled();
	  if (st22 == true)
	  {
      System.out.println("PublishNowButton is enabled when we Edit a volunteer message in the Volunteer Message page.TestCasePassed");
	  }
	  else
	  {
		  System.out.println("PublishNow Button is disabled when we Edit a volunteer message in the Volunteer Message page.TestCasefailed");
	  }
	  boolean st23= Savebutton.isEnabled();
	  if (st23 == true)
	  {
      System.out.println("Save button is enabled when we Edit a volunteer message in the Volunteer Message page.TestCasepass");
	  }
	  else
	  {
		  System.out.println("Save button is disabled when we Edit a volunteer message in the Volunteer Message page.TestCasefail");
	  }
	  
	  boolean st24= CancelButton.isEnabled();
	  if (st24 == true)
	  {
      System.out.println("CancelButton is enabled when we Edit a volunteer message in the Volunteer Message page.TestCasepassed");
	  }
	  else
	  {
		  System.out.println("CancelButton is disabled when we  Edit a volunteer message in the Volunteer Message page.TestCasefailed");
          }
          boolean st25= Goback.isEnabled();
	  if (st25 == true)
	  {
      System.out.println("Goback button is enabled when we Edit a volunteer message in the Volunteer Message page.TestCasepassed");
	  }
	  else
	  {
		  System.out.println("Goback is disabled when we Edit a volunteer message in the Volunteer Message page.TestCasefailed");
          }
}
   
}


