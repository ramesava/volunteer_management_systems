package com.scholastic.pagerepo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.genericutils.GenericActions;

public class AddContactDetails {
	public WebDriver driver;


    @FindBy(xpath="//button[contains(text(), 'Save & Add Another')]")
	private WebElement AddAnother;
    @FindBy(xpath="//a[contains(text(),'Cancel')]")
	private WebElement Cancel;
	@FindBy(xpath="//button[contains(text(), 'Save')]")
	private WebElement Save;
	@FindBy(xpath="//input[@name='firstName']")
	private WebElement Firstname;
	@FindBy(xpath="//input[@name='lastName']")
	private WebElement LastName;
	@FindBy(xpath="//input[@name='emailaddress']")
	private WebElement email;
	@FindBy(xpath="//input[@type='checkbox']")
	private WebElement Toolkitcheckbox;
	@FindBy(xpath="//input[@name='phone']")
	private WebElement phone;
	@FindBy(xpath="//input[@name='address1']")
	private WebElement Address1;
	@FindBy(xpath="//input[@name='address2']")
	private WebElement Address2;
	@FindBy(xpath="//input[@name='city']")
	private WebElement city;
	@FindBy(xpath="//select[@name='state']")
	private WebElement state;
	@FindBy(xpath="//input[@name='postalCode']")
	private WebElement postalcode;
	@FindBy(xpath="//p[contains(text(),'First Name is required field.')]")
	private WebElement Firstnameblankerrormsg;
	@FindBy(xpath="//p[contains(text(),'Last Name is required field.')]")
	private WebElement Lastnameblankerrormsg;
	@FindBy(xpath="//p[contains(text(),'Email address is required field.')]")
	private WebElement Emailaddressblankerrormsg;
	
	public AddContactDetails(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void addcontacts() throws InterruptedException
	{
		Firstname.sendKeys(GenericActions.getExcelData("Sheet3", 0, 0));
		LastName.sendKeys(GenericActions.getExcelData("Sheet3", 0, 1));;
		email.sendKeys(GenericActions.getExcelData("Sheet3", 0, 2));;
		//Toolkitcheckbox.click();
		phone.sendKeys("9886265769");
		Address1.sendKeys("7BoringStreet");
		Address2.sendKeys("Eastwood");
		city.sendKeys("Newyork");
		GenericActions.singleselectdropdown( state, 2);
		Thread.sleep(3000);
		postalcode.sendKeys("32423");
		boolean status=driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).isEnabled();
		if (status==true)
		{
			System.out.println("Save button is enabled");
		}
		
		else
			
		{
			System.out.println("Save button is disabled");
		}
		
		 
		 Save.click();
		
		//System.out.println("**********enabled?????????:    "+driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).isEnabled());
		//driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).click();
		//Save.click();
		//Save.click();
		//JavascriptExecutor js =(JavascriptExecutor)driver;
	   // WebElement ele = driver.findElement(By.xpath("//button[contains(text(), 'Save')]"));
	    //String script ="arguments[0].click();";
		//js.executeScript(script,ele );
		
		
		
		}
	
	public void addcontacts1() throws InterruptedException
	{
		Firstname.sendKeys(GenericActions.getExcelData("Sheet3", 1, 0));
		LastName.sendKeys(GenericActions.getExcelData("Sheet3", 1, 1));;
		email.sendKeys(GenericActions.getExcelData("Sheet3", 1, 2));;
		Toolkitcheckbox.click();
		phone.sendKeys("7634598746");
		Address1.sendKeys("89 Edward");
		Address2.sendKeys("Street25");
		city.sendKeys("Newyork");
		GenericActions.singleselectdropdown( state, 1);
		Thread.sleep(3000);
		postalcode.sendKeys("32425");
		AddAnother.click();
		//AddAnother.click();
		
		}
	public void addcontacts2() throws InterruptedException
	{
		Firstname.sendKeys(GenericActions.getExcelData("Sheet3", 2, 0));
		LastName.sendKeys(GenericActions.getExcelData("Sheet3", 2, 1));;
		email.sendKeys(GenericActions.getExcelData("Sheet3", 2, 2));;
		
		Cancel.click();
		}

   public void Editcontacts() throws InterruptedException
   {  
	   Firstname.clear();
	   Firstname.sendKeys("Uniquename");
	   Thread.sleep(2000);
	   Save.click();
	   Thread.sleep(3000);
	   if(driver.findElements(By.xpath("//a[contains(text(),'Uniquename')]")).size()==0)
		{
			System.out.println("Contact Name not edited through Edit link  in the overlay  and Edit  button under volunteer history. Test case failed");
		}
		else
		{
			System.out.println("Contact Name  edited through Edit link in the overlay and Edit button under volunteer history. Test case pass");
		}
		
	   
}
   
   
   public void EditcontactsCancel() throws InterruptedException
   {
	   Firstname.clear();
	   Firstname.sendKeys("EditContactscancel");
	   Thread.sleep(2000);
	   Cancel.click();

if(driver.findElements(By.xpath("//a[contains(text(),'EditContactscancel')]")).size()==1)
	{
		System.out.println("Clicking on Edit contact and then cancelling the edit operation actually edits the contact.Test case failed");
	}
	else
	{
		System.out.println("Clicking on Edit contact and then cancelling the edit operation does not edit the contact.Test case pass");
	}
	
	   
   }
   public void Allfieldsblank1() throws InterruptedException
   {
	   Firstname.sendKeys("");
	   LastName.sendKeys("");
	   email.sendKeys("");
	   phone.sendKeys("");
		Address1.sendKeys("");
		Address2.sendKeys("");
		city.sendKeys("");
	postalcode.sendKeys("");
	Save.click();
	Thread.sleep(5000);
	boolean st1=Firstnameblankerrormsg.isDisplayed();
	if (st1 ==true)
	{
	 System.out.println("When all fields are blank and user hits save button in add contacts page, first name is required field is displayed.TestCasePassed");
	}
	else
	{
		System.out.println("When all fields are blank and user hits save button in add contacts page, first name is requiredfield is not  displayed.TestCaseFailed");	
	}
	boolean st2=Lastnameblankerrormsg.isDisplayed();
	if (st2 ==true)
	{
	 System.out.println("When all fields are blank and user hits save button in add contacts page, last name is required field is displayed.TestCasePassed");
	}
	else
	{
		System.out.println("When all fields are blank and user hits save button in add contacts page, last name is required is not  displayed.TestCaseFailed");	
	}
	
	//boolean st3=Emailaddressblankerrormsg.isDisplayed();
	//if (st3 ==true)
	//{
	 //System.out.println("When all fields are blank and user hits save button in add contacts page, email is required field  is displayed.TestCasePassed");
	//}
	//else
	//{
		//System.out.println("When all fields are blank and user hits save button in add contacts page, email is required field is not  displayed.TestCaseFailed");	
	//}
   }
	
	public void Allfieldsblank2() throws InterruptedException
	   {
		   Firstname.sendKeys("");
		   LastName.sendKeys("");
		   email.sendKeys("");
		   phone.sendKeys("");
			Address1.sendKeys("");
			Address2.sendKeys("");
			city.sendKeys("");
		postalcode.sendKeys("");
		AddAnother.click();
		Thread.sleep(5000);
		boolean st4=Firstnameblankerrormsg.isDisplayed();
		if (st4 ==true)
		{
		 System.out.println("When all fields are blank and user hits save and button in add contacts page, first name is required field is displayed.TestCasePassed");
		}
		else
		{
			System.out.println("When all fields are blank and user hits save and add  button in add contacts page, first name is required field is not  displayed.TestCaseFailed");	
		}
		boolean st5=Lastnameblankerrormsg.isDisplayed();
		if (st5 ==true)
		{
		 System.out.println("When all fields are blank and user hits save and add  button in add contacts page, last name is required field is displayed.TestCasePassed");
		}
		else
		{
			System.out.println("When all fields are blank and user hits save and button in add contacts page, last name is required is not  displayed.TestCaseFailed");	
		}
		
		//boolean st6=Emailaddressblankerrormsg.isDisplayed();
		//if (st6 ==true)
		//{
		 //System.out.println("When all fields are blank and user hits save and add  button in add contacts page, email is required field  is displayed.TestCasePassed");
		//}
		//else
		//{
			//System.out.println("When all fields are blank and user hits save and add button in add contacts page, email is required field is not  displayed.TestCaseFailed");	
		//}
	   }
		
		public void Firstnamefieldblank1() throws InterruptedException
		   {
			   Firstname.sendKeys("");
			   LastName.sendKeys("John");
			   email.sendKeys("john@gmail.com");
			   phone.sendKeys("9886565769");
				Address1.sendKeys("Address1");
				Address2.sendKeys("Address2");
				city.sendKeys("Newyork");
			postalcode.sendKeys("12345");
			Save.click();
			Thread.sleep(5000);
			boolean st7=Firstnameblankerrormsg.isDisplayed();
			if (st7 ==true)
			{
			 System.out.println("When Firstname field is blank and user hits save in add contacts page, first name is required field is displayed.TestCasePassed");
			}
			else
			{
				System.out.println("When Firstname field is blank and user hits save  in add contacts page, first is required field is not  displayed.TestCaseFailed");	
			}

LastName.clear();
email.clear();
phone.clear();
Address1.clear();
Address2.clear();
city.clear();
postalcode.clear();
			
		   }
			
			public void Firstnamefieldblank2() throws InterruptedException
			   {
				   Firstname.sendKeys("");
				   LastName.sendKeys("John");
				   email.sendKeys("john1@gmail.com");
				   phone.sendKeys("9886565769");
					Address1.sendKeys("Address1");
					Address2.sendKeys("Address2");
					city.sendKeys("Newyork");
				postalcode.sendKeys("12345");
				AddAnother.click();
				Thread.sleep(5000);
				boolean st8=Firstnameblankerrormsg.isDisplayed();
				if (st8 ==true)
				{
				 System.out.println("When Firstname field is blank and user hits save&add  in add contacts page, first name is required field is displayed.TestCasePassed");
				}
				else
				{
					System.out.println("When Firstname field is blank and user hits save&add  in add contacts page, first is required field is not  displayed.TestCaseFailed");	
				}
				LastName.clear();
				email.clear();
				phone.clear();
				Address1.clear();
				Address2.clear();
				city.clear();
				postalcode.clear();
				
			   }
	
			public void Lastnamefieldblank1() throws InterruptedException
				   {
					   Firstname.sendKeys("Roy");
					   LastName.sendKeys("");
					   email.sendKeys("roy@gmail.com");
					   phone.sendKeys("9886565769");
						Address1.sendKeys("Address1");
						Address2.sendKeys("Address2");
						city.sendKeys("Newyork");
					postalcode.sendKeys("12345");
					Save.click();
					Thread.sleep(5000);
					boolean st9=Lastnameblankerrormsg.isDisplayed();
					if (st9 ==true)
					{
					 System.out.println("When Lastname field is blank and user hits save in add contacts page, last name is required field is displayed.TestCasePassed");
					}
					else
					{
						System.out.println("When Lastname field is blank and user hits save  in add contacts page, last is required field is not  displayed.TestCaseFailed");	
					}
					LastName.clear();
					email.clear();
					phone.clear();
					Address1.clear();
					Address2.clear();
					city.clear();
					postalcode.clear();
				   }
			
			public void Lastnamefieldblank2() throws InterruptedException
			   {
				   Firstname.sendKeys("Roy");
				   LastName.sendKeys("");
				   email.sendKeys("roy1@gmail.com");
				   phone.sendKeys("9886565769");
					Address1.sendKeys("Address1");
					Address2.sendKeys("Address2");
					city.sendKeys("Newyork");
				postalcode.sendKeys("12345");
				AddAnother.click();
				Thread.sleep(5000);
				boolean st10=Lastnameblankerrormsg.isDisplayed();
				if (st10 ==true)
				{
				 System.out.println("When Lastname field is blank and user hits save& add in add contacts page, last name is required field is displayed.TestCasePassed");
				}
				else
				{
					System.out.println("When Lastname field is blank and user hits save&add  in add contacts page, last is required field is not  displayed.TestCaseFailed");	
				}
				LastName.clear();
				email.clear();
				phone.clear();
				Address1.clear();
				Address2.clear();
				city.clear();
				postalcode.clear();
			   }
			//public void emailfieldblank1() throws InterruptedException
			   //{
				   //Firstname.sendKeys("Roy");
				   //LastName.sendKeys("Sam");
				  // email.sendKeys("");
				  // phone.sendKeys("9886565769");
					//Address1.sendKeys("Address1");
					//Address2.sendKeys("Address2");
					//city.sendKeys("Newyork");
				//postalcode.sendKeys("12345");
				//Save.click();
				///Thread.sleep(5000);
				//boolean st11=Emailaddressblankerrormsg.isDisplayed();
				//if (st11 ==true)
				//{
				 //System.out.println("When Email field is blank and user hits save in add contacts page, email is required field is displayed.TestCasePassed");
				//}
				//else
				//{
					//System.out.println("When Email field is blank and user hits save  in add contacts page, email is required field is not  displayed.TestCaseFailed");	
				//}
				//LastName.clear();
				//email.clear();
				//phone.clear();
				//Address1.clear();
				//Address2.clear();
				//city.clear();
				//postalcode.clear();
			   //}
			
			//public void emailfieldblank2() throws InterruptedException
			   //{
				  // Firstname.sendKeys("Royes");
				  // LastName.sendKeys("Sammy");
				  // email.sendKeys("");
				  // phone.sendKeys("9886565769");
					//Address1.sendKeys("Address1");
					//Address2.sendKeys("Address2");
					//city.sendKeys("Newyork");
				//postalcode.sendKeys("12345");
				//AddAnother.click();
				//Thread.sleep(5000);
				//boolean st12=Emailaddressblankerrormsg.isDisplayed();
				//if (st12 ==true)
				//{
				// System.out.println("When Email field is blank and user hits save& add in add contacts page,email is required field is displayed.TestCasePassed");
				//}
				//else
				//{
					//System.out.println("When Lastname field is blank and user hits save&add  in add contacts page, email is required field is not  displayed.TestCaseFailed");	
				//}
				//LastName.clear();
				//email.clear();
				//phone.clear();
				//Address1.clear();
				//Address2.clear();
				//city.clear();
				//postalcode.clear();
			  // }
	   
	   
			public void addcontactstodeletefromvolunteerhistory() throws InterruptedException
			{
				Firstname.sendKeys(GenericActions.getExcelData("Sheet3", 5, 0));
				LastName.sendKeys(GenericActions.getExcelData("Sheet3", 5, 1));;
				email.sendKeys(GenericActions.getExcelData("Sheet3", 5, 2));;
				//Toolkitcheckbox.click();
				phone.sendKeys("9886265769");
				Address1.sendKeys("7BoringStreet");
				Address2.sendKeys("Eastwood");
				city.sendKeys("Newyork");
				GenericActions.singleselectdropdown( state, 2);
				Thread.sleep(1000);
				postalcode.sendKeys("32423");
				
				 
				 Save.click();
				
				//System.out.println("**********enabled?????????:    "+driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).isEnabled());
				//driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).click();
				//Save.click();
				//Save.click();
				//JavascriptExecutor js =(JavascriptExecutor)driver;
			   // WebElement ele = driver.findElement(By.xpath("//button[contains(text(), 'Save')]"));
			    //String script ="arguments[0].click();";
				//js.executeScript(script,ele );
				
				
				
				}
			
	   
			public void addcontacts3() throws InterruptedException
			{
				Firstname.sendKeys("Barry");
				LastName.sendKeys("Bossist");;
				email.sendKeys("barry@gmail.com");;
				Toolkitcheckbox.click();
				phone.sendKeys("9886265769");
				Address1.sendKeys("7BoringStreet");
				Address2.sendKeys("Eastwood");
				city.sendKeys("Newyork");
				GenericActions.singleselectdropdown( state, 2);
				Thread.sleep(3000);
				postalcode.sendKeys("32423");
				
				 
				 Save.click();
				
				//System.out.println("**********enabled?????????:    "+driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).isEnabled());
				//driver.findElement(By.xpath("//button[contains(text(), 'Save')]")).click();
				//Save.click();
				//Save.click();
				//JavascriptExecutor js =(JavascriptExecutor)driver;
			   // WebElement ele = driver.findElement(By.xpath("//button[contains(text(), 'Save')]"));
			    //String script ="arguments[0].click();";
				//js.executeScript(script,ele );
				
				
				
				}
	   
	   
	   
	   
	}
