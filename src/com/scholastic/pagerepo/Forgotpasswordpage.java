package com.scholastic.pagerepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Forgotpasswordpage{
	public WebDriver driver;
	@FindBy(xpath="//input[@name='email']")
	private WebElement Email;
	@FindBy(xpath="//a[@id ='forgotAccountSubmit']")
	private WebElement Submit;
	@FindBy(xpath="//a[@id ='forgotAccountCancel']")
	private WebElement Cancel;
	@FindBy(xpath="//a[@title='Email Address Update']")
	private WebElement Customercare;
	
	
	public Forgotpasswordpage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void forgotpassword1() throws InterruptedException
	{
		Email.sendKeys("");
		Thread.sleep(2000);
		Submit.click();
	}
	
    public void forgotpassword2() throws InterruptedException
    {
    	Email.sendKeys("ddddddd@relevancelab.com");
		Thread.sleep(2000);
		Submit.click();
		Email.clear();
    }
    
    public void forgotpassword3() throws InterruptedException
    {
    	Email.sendKeys("ddddddd");
		Thread.sleep(2000);
		Submit.click();
		
    }
    
    public void forgotpassword4() throws InterruptedException
    {
    	Email.sendKeys("prasanjit.choudhury@relevancelab.com");
		Thread.sleep(2000);
		Submit.click();
    }
    
    public void forgotpassword5() throws InterruptedException
    {
    	Cancel.click();
    }


}
