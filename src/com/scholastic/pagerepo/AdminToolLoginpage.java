package com.scholastic.pagerepo;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.genericutils.GenericActions;

public class AdminToolLoginpage {
	
	
	public WebDriver driver;
	@FindBy(xpath="//input[@name='username']")
	private WebElement usernameTextbox;
	@FindBy(xpath="//input[@name='password']")
	private WebElement passwordbox;
	@FindBy(xpath="//button[@type='submit']")
	private WebElement LoginButton;
	@FindBy(xpath="//button[contains(text(), 'Ok')]")
	private WebElement OkPopup;
	@FindBy(xpath="//span[contains(text(), 'Username')]")
	private WebElement Usernamecantbeblank;
	@FindBy(xpath="//span[contains(text(), 'Password')]")
	private WebElement Passwordcantbeblank;
	@FindBy (xpath="//p[contains(text(),'Incorrect username')]")
	private WebElement IncorrectUsernamepassword;
	
	
	
	
	
	public AdminToolLoginpage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
		
	
	//click Login 	without entering any login credentials
		public void login1()
		{
			
			LoginButton.click();
			if(Usernamecantbeblank.isDisplayed()&&Passwordcantbeblank.isDisplayed())
			{ 
				System.out.println("Proper error message isdisplayed if user clicks login without entering username and password.Testcasepass");
			}
			
			else
				System.out.println("Error message displayed is not proper if user clicks login without entering username and password.Testcasefail");
			
			
			
		}
		
		public void login5() throws InterruptedException
		{
			String pwd =GenericActions.getExcelData("login", 0, 1);
			passwordbox.sendKeys(pwd);
			LoginButton.click();
			Thread.sleep(3000);
			passwordbox.clear();
			
		}
		
		public void login6()
		{
			String un =GenericActions.getExcelData("login", 0, 0);
			usernameTextbox.sendKeys(un);
			LoginButton.click();
			
		}
		
	
	
	
	//Login with Valid login credentials
	public void login() throws InterruptedException
	{
		String un =GenericActions.getExcelData("login", 0, 0);
		usernameTextbox.sendKeys(un);
		String pwd =GenericActions.getExcelData("login", 0, 1);
		passwordbox.sendKeys(pwd);
		LoginButton.click();
		Thread.sleep(5000);
		String url =driver.getCurrentUrl();
		Thread.sleep(3000);
		if(url.equals("https://vms-qa.scholastic.com/#/admin/home"))
		{
		 System .out.println("Upon successful login, user is navigated to home page");
		 
		}
		
		else
		{
			System .out.println("Error during login");
		}
		
		
		
		
		
		
		}
	
	//click Login with InvalidUserName and Invalid Password
	public void login2() throws InterruptedException, IOException
	{
		String un =GenericActions.getExcelData("login", 2, 0);
		usernameTextbox.sendKeys(un);
		String pwd =GenericActions.getExcelData("login", 2, 1);
		passwordbox.sendKeys(pwd);
		LoginButton.click();
		Thread.sleep(3000);
		if(IncorrectUsernamepassword.isDisplayed())
		{
			System.out.println("Proper error message isdisplayed if user clicks login after entering invalid username and password");
			}
		else
		{
			System.out.println("Error message isdisplayed is not proper if user clicks login after entering invalid username and password");
		}
       
        GenericActions.capturescreenshots(driver, "Screenhot for Logging unsuccessful with invalid username and invalid password");
	    OkPopup.click();
	    usernameTextbox.clear();
		passwordbox.clear();
		
		
		
		
	}
	
	//click Login with ValidUsername and Invalid password
	
	public void login3() throws InterruptedException, IOException
	{
		String un =GenericActions.getExcelData("login", 4, 0);
		usernameTextbox.sendKeys(un);
		String pwd =GenericActions.getExcelData("login", 4, 1);
		passwordbox.sendKeys(pwd);
		LoginButton.click();
		Thread.sleep(3000);
		if(IncorrectUsernamepassword.isDisplayed())
		{
			System.out.println("Proper error message isdisplayed if user clicks login after entering valid username and invalid password");
			}
		else
		{
			System.out.println("Error message isdisplayed is not proper if user clicks login after entering valid username and invalid password");
		}
		
		GenericActions.capturescreenshots(driver, "Screenshot for Logging unsuccessful with valid username and invalid password");
		OkPopup.click();
		usernameTextbox.clear();
		passwordbox.clear();
		
	}
    
	//click Login with invalidUsername and valid password
	public void login4() throws InterruptedException, IOException
	{
		String un =GenericActions.getExcelData("login", 6, 0);
		usernameTextbox.sendKeys(un);
		String pwd =GenericActions.getExcelData("login", 6, 1);
		passwordbox.sendKeys(pwd);
		LoginButton.click();
		Thread.sleep(3000);
		Thread.sleep(3000);
		if(IncorrectUsernamepassword.isDisplayed())
		{
			System.out.println("Proper error message isdisplayed if user clicks login after entering invalid username and valid password");
			}
		else
		{
			System.out.println("Error message isdisplayed is not proper if user clicks login after entering invalid username and valid password");
		}
		GenericActions.capturescreenshots(driver, "Screenshot for Logging unsuccessful with invalid username and valid password");
		OkPopup.click();
		usernameTextbox.clear();
		passwordbox.clear();
		
		
	}
	
}
