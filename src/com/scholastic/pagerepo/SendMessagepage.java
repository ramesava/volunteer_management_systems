package com.scholastic.pagerepo;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.genericutils.GenericActions;

public class SendMessagepage {
	public WebDriver driver;
	@FindBy(xpath="//ul[@class='select2-choices']")
	private WebElement Recipienttext;
	@FindBy(xpath=".//*[@id='select2-drop-mask']")
	private WebElement Recipienttext_2;
	
	
	@FindBy(xpath="//textarea[@ng-model='email.message']")
	private WebElement Messagetext;
	@FindBy(xpath="//input[@name='Send']")
	private WebElement SendEmail;
	@FindBy(xpath="//a[contains(text(),'Cancel')]")
	private WebElement CancelEmail;
	@FindBy(xpath="//button[contains(text(),'Okay')]")
	private WebElement Okay;
	@FindBy(xpath="//a[contains(text(),'Add')]")
	private WebElement AddnewEmailaddress;
	
	public SendMessagepage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void Sendemail() throws InterruptedException, IOException
	{
		System.out.println("#############");
		Recipienttext.click();
		System.out.println("$$$$$$$$$$$$$$");
		Thread.sleep(3000);
		Recipienttext.click();
		//Thread.sleep(3000);
		Recipienttext.sendKeys("prasanjit.choudhury@relevancelab.com");
		Thread.sleep(9000);
		AddnewEmailaddress.click();
		
		 Messagetext.sendKeys("This is a test email");
		 Thread.sleep(3000);
		 SendEmail.click();
		 Thread.sleep(2000);
		 GenericActions.capturescreenshots(driver, "Screenhot for Logging unsuccessful with invalid username and invalid password");
		 Okay.click();
		
	}
	
	public void Cancelemail() throws InterruptedException, IOException
	{
		Recipienttext.sendKeys("prasanjit.choudhury@relevancelab.com");
		Thread.sleep(9000);
		AddnewEmailaddress.click();
		 Messagetext.sendKeys("This is a test email");
		 Thread.sleep(3000);
		 CancelEmail.click();
		 
}

	
	public void SendEmailwithoutrecipentandmessage() throws InterruptedException, IOException
	{
		Recipienttext.sendKeys("");
		 Messagetext.sendKeys("");
		 Thread.sleep(3000);
		 SendEmail.click();
		 

}
	public void CancelEmailwithoutrecipentandmessage() throws InterruptedException, IOException
	{
		Recipienttext.sendKeys("");
		 Messagetext.sendKeys("");
		 Thread.sleep(3000);
		 CancelEmail.click();
		 Thread.sleep(20000);
		 GenericActions.capturescreenshots(driver, "Screenhot for Logging unsuccessful with invalid username and invalid password");
		 Okay.click();
	}

	
}


