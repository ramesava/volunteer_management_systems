package com.scholastic.pagerepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ForgotpasswordSuccess {
	

	public WebDriver driver;
	@FindBy(xpath="//a[@id ='forgotAccountCancel2']")
	private WebElement Return;

	
	public ForgotpasswordSuccess(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
}

   public  void clickreturn()
   {
	   Return.click();
   }


}