package com.scholastic.pagerepo;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.genericutils.GenericActions;

public class MyContactsPage {
	
	public WebDriver driver;
	@FindBy(xpath="//a[contains(text(),'Add A Contact')]")
	private WebElement Addacontact;
	@FindBy(xpath="//label[@for ='squaredOne0']")
	private WebElement firstcheckbox;
	@FindBy (xpath="//label[@for ='squaredOne1']")
	private WebElement secondcheckbox;
	@FindBy (xpath="//label[@for ='squaredOne2']")
	private WebElement thirdcheckbox;
	@FindBy (xpath="//label[@for ='squaredOne3']")
	private WebElement fourthcheckbox;
	@FindBy (xpath="//label[@for ='squaredOne4']")
	private WebElement fifthcheckbox;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/div[7]/div[2]/button")
	private WebElement ImportPopupclosebutton;
	
	
	
	
	@FindBy(xpath="//a[contains (text(),'Edit Contact')]")
	private WebElement Edit;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[1]/section/ul/li[5]/a")
	private WebElement Delete1;
	@FindBy(xpath=".//*[@id='content']/div[2]/div/div/section[2]/div/div[2]/ul/li[2]/section/ul/li[5]/a")
	private WebElement Delete2;
	@FindBy(xpath=" /html/body/div[2]/div[1]/div[1]/section[2]/div[1]/div[2]/ul/li[3]/section[1]/ul[1]/li[5]/a")
	private WebElement Delete3;
	//@FindBy(xpath="/html/body/div[2]/div[1]/div[1]/div [2]/footer[1]/button[2]")
	//private WebElement Deletebutton2ndcheckbox;
	@FindBy(xpath=".//*[@id='content']/div[2]/div/div/div[5]/footer/button[2]")
	private WebElement Deletebutton1stcheckbox;
	@FindBy(xpath="//div[3]/footer/button[2]")
	private WebElement Deletebutton2ndcheckbox;
	@FindBy(xpath=".//*[@id='content']/div[2]/div/div/div[3]/footer/button[2]")
	private WebElement Deletebutton3rdcheckbox;
	@FindBy(xpath="//button[contains(text(),'Manage Groups')]")
	private WebElement ManageGroup;
	@FindBy(xpath="//input[@placeholder='New Group Name']")
	private WebElement AddNewgroupfield;
	@FindBy(xpath="//button[contains(text(),'Save')]")
	private WebElement SaveGroup;
	@FindBy(xpath="//button[contains(text(),'Cancel')]")
	private WebElement CancelGroup;
	@FindBy (xpath="(//span[2]/span)[3]")
	private WebElement Deletegroup1;
	@FindBy (xpath="//li[3]/span/span")
	private WebElement Deletegroup2;
	@FindBy (xpath="//li[6]/span/span")
	private WebElement Deletegroup5;
	@FindBy (xpath="//li[5]/span/span")
	private WebElement Deletegroup4;
	@FindBy (xpath="//a[contains(text(),'Scholastic Inc.')]")
	private WebElement ScholasticInclink;
	@FindBy (xpath="//a[contains(text(),'Help')]")
	private WebElement Helplink;
	@FindBy (xpath="//a[contains(text(),'About Us')]")
	private WebElement AboutUslink;
	@FindBy (xpath="//a[contains(text(),'Privacy Policy')]")
	private WebElement PrivacyPolicylink;
	@FindBy (xpath="//a[contains(text(),'Terms of Use')]")
	private WebElement TermsOfUselink;
	@FindBy (xpath="//li[contains(text(),'� � & � 2015 Scholastic Inc. All Rights Reserved.')]")
	private WebElement RightsReservedtext;
	@FindBy (xpath="(//a[@class='slideDown'])[1]")
	private WebElement FirstContactlink;
	
	@FindBy (xpath="(//a[@class='slideDown'])[2]")
	private WebElement SecondContactlink;
	
	@FindBy (xpath="//a[contains(text(),'TestPrasanjit1 TestPrasanjit1')]")
	private WebElement TestPrasanjit1;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[1]/div/div/div[2]/button[3]")
	private WebElement EditaContact1stbutton;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[1]/div/div/div[2]/button[4]")
	private WebElement DeleteContact1stbutton;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[2]/div/div/div[2]/button[4]")
	private WebElement DeleteContact2ndbutton;
	
	
	@FindBy (xpath="//div[4]/footer/button[2]")
	private WebElement Deletepopup1stcontact;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/div[6]/footer/button[2]")
	private WebElement Deletepopup2ndcontact;
	
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[2]/section/ul/li[5]/a")
	private WebElement DeleteMultipleSelected;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/div[5]/footer/button[2]")
	private WebElement DeleteMultiplebutton;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/div[5]/footer/button[1]")
	private WebElement Cancelbuttonpopup1stcheckbox;
	@FindBy (xpath="//*[@id='content']/div[2]/div/div/div[6]/footer/button[1]")
	private WebElement Cancelbuttonpopup1stcheckboxvolunteerhistory;
	@FindBy (xpath="//a[contains(text(),'All Contacts')]")
	private WebElement AllContactslink;
	@FindBy (xpath="//label[@ng-model='checkAll']")
	private WebElement SelectAllCheckbox;
	@FindBy (xpath="//a[contains(text(),'Delete All')]")
	private WebElement DeleteAll;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/div[2]/footer/button[2]")
	private WebElement DeleteAllbutton;
	@FindBy (xpath=" //a[contains(text(),'qa group2')]")
	private WebElement Qagroup2;
	@FindBy (xpath="//a[contains(text(),'Send an Email')]")
	private WebElement SendMessagebutton;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[4]/ul/li[2]/div/label/input")
	private WebElement Addtogroup1stcheckbox;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[4]/ul/li[3]/div/label/input")
	private WebElement Addtogroup2ndcheckbox;
	@FindBy (xpath="(//input[@type='checkbox'])[10]")
	private WebElement Addtogroup3rdcheckbox;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[1]/section/ul/li[3]/a")
	private WebElement Add1togrouplink;
	@FindBy (xpath="//button[@ng-disabled='disableAddToGroupSave()']")
	private WebElement AddtoGroupSavebutton;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[2]/section/ul/li[3]/a")
	private WebElement Add2togrouplink;
	@FindBy (xpath="//a[contains(text(),'Add 5 to Group')]")
	private WebElement AddAlltogrouplink;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/aside/div[1]/div/div[2]/ul/li[2]/a")
	private WebElement FirstGroupleft;
	@FindBy (xpath="(//a[@ng-click='updateContactList(grp)'])[3]")
	private WebElement SecondGroupleft;
	@FindBy (xpath="(//a[@ng-click='updateContactList(grp)'])[4]")
	private WebElement ThirdGroupleft;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[1]/section/ul/li[4]/a")
	private WebElement RemovefromGroup1;
	@FindBy (xpath=".//*[@id='content']/div[2]/div/div/section[3]/div[1]/div[2]/ul/li[2]/section/ul/li[4]/a")
	private WebElement RemovefromGroup2;
	@FindBy (xpath="(//a[contains(text(),'Remove from Group')])[5]")
	private WebElement RemovefromGroup5;
	@FindBy (xpath="//select")
	private WebElement Sort;
	@FindBy (xpath="//button[@data-slide-to='1']")
	private WebElement HelpNext1;
    @FindBy (xpath="//button[@data-slide-to='2']")
	private WebElement HelpNext2;
    @FindBy (xpath="//button[@data-slide-to='3']")
	private WebElement HelpNext3;
    @FindBy (xpath="(//a[@ng-click='CloseCarousel()'])[4]")
    private WebElement HelpClose;
    
    @FindBy (xpath="//a[contains(text(),'Current Fair Volunteers')]")
    private WebElement CurrentFairVolunteers;
    
    @FindBy (xpath="//a[contains(text(),'Current Fair Toolkit Users')]")
    private WebElement CurrentFairToolkitUsers;
    @FindBy (xpath="//a[@ng-click='goToExport()']")
    private WebElement Export;
    @FindBy (xpath="//a[@ng-click='goToImport()']")
    private WebElement Import;
	
	
	
	


 public MyContactsPage(WebDriver driver) {
	this.driver = driver;
	PageFactory.initElements(driver, this);
}

public void AddContact()
{
	
	Addacontact.click();
	
}

public void EditContact() throws InterruptedException
{
	firstcheckbox.click();
	Edit.click();
	Thread.sleep(2000);
	 String url=driver.getCurrentUrl();
		if (url.equals("https://vms-qa.scholastic.com/#/contacts/add/502/370"))
		{
			System.out.println("User navigated to Edit Contact Screen by clicking Edit link in the overlay.TestCase Pass");
		}
		
		else
	 
	 System.out.println("User is not  navigated to Edit Contact Screen by clicking Edit link in the overlay.TestCase Failed");
	 
}
	


public void DeleteContact() throws InterruptedException
{
	
	firstcheckbox.click();
	 Thread.sleep(2000);
	 Delete1.click();
	 Thread.sleep(2000);
	Deletebutton1stcheckbox.click();
	Thread.sleep(3000);
	if(driver.findElements(By.xpath("//a[contains(text(),'Aabey')]")).size()==0)
	{
	System.out.println("Single contact deleted through popup overlay.Testcase passed");
	}
	
	else
	{
	System.out.println("Single contact not deleted through popup overlay.Testcase failed");	
	}

	
}

public void AddGroup() throws InterruptedException

{
	ManageGroup.click();
	AddNewgroupfield.sendKeys("UniqueGroup7");
	Thread.sleep(4000);
	SaveGroup.click();
	Thread.sleep(2000);
	if(driver.findElements(By.xpath("//a[contains(text(),'UniqueGroup7')]")).size()==0)
	{
		System.out.println("New Group not added. Test case failed");
	}
	else
	{
		System.out.println("New Group successfully added. Test case pass");
	}
	
}

public void CancelGroup() throws InterruptedException
{
	ManageGroup.click();
	
	Thread.sleep(2000);
	AddNewgroupfield.sendKeys("UniqueGroup9");
	Thread.sleep(4000);
	CancelGroup.click();
	
	if(driver.findElements(By.xpath("//a[contains(text(),'UniqueGroup9')]")).size()==0)
	{
		System.out.println("New Group not added on click of cancel in Manage Groups. Test case pass");
	}
	else
	{
		System.out.println("New Group  added on click of cancel in Manage Groups. Test case fail");
	}
}

 public void DeleteGroup() throws InterruptedException
 {
	 ManageGroup.click();
	 Thread.sleep(2000);
	 Deletegroup1.click();
	 Thread.sleep(2000);
	 AddNewgroupfield.clear();
	 
	 SaveGroup.click();
	 Thread.sleep(4000);
	 if(driver.findElements(By.xpath("//a[contains(text(),'UniqueGroup7')]")).size()==0)
		{
			System.out.println("New Group successfully deleted. Test case pass");
		}
		else
		{
			System.out.println("New Group not deleted. Test case fail");
		}
	 
	 
 }
 
 public void headerfoooter()
 {
	 ScholasticInclink.click();
	 Helplink.click();
	 AboutUslink.click();
	 PrivacyPolicylink.click();
	 TermsOfUselink.click();
	 boolean status =RightsReservedtext.isDisplayed();
	 if (status == true)
	 {
         System.out.println("RightsReservedtext is displayed at the footer of My Contacts page");
	 }
         else
   	  
   	 {
   		  System.out.println("RightsReservedtext is displayed at the footer of My Contacts page.TestCasefail");
   	  }
 }
	 
	 public void EditContact1() throws InterruptedException
	 {
		 FirstContactlink.click();
		 Thread.sleep(3000);
		 EditaContact1stbutton.click();
		 Thread.sleep(2000);
		 String url=driver.getCurrentUrl();
			if (url.equals("https://vms-qa.scholastic.com/#/contacts/add/502/370"))
			{
				System.out.println("User navigated to Edit Contact Screen by clicking Edit button under volunteer history.TestCasePass");
			}
			
			else
		 
		 System.out.println("User is not  navigated to Edit Contact Screen by clicking Edit button under volunteer history.TestCaseFail");
		 
	 }
	 
	 public void DeleteMultipleContacts() throws InterruptedException
	 {
		 firstcheckbox.click();
		 secondcheckbox.click();
		 DeleteMultipleSelected.click();
		 Thread.sleep(3000);
		 DeleteMultiplebutton.click();
		 Thread.sleep(1000);
		 
			 System.out.println("Multiple deletion successful.Testcase pass");
		
		 }
		 
	
	 
	 public void DeleteContact1() throws InterruptedException
	 {
		 SecondContactlink.click();
		 Thread.sleep(3000);
		 DeleteContact2ndbutton.click();
		 Thread.sleep(3000);
		 Deletepopup2ndcontact.click();
		 Thread.sleep(3000);
		 if (driver.findElements(By.xpath("//label[@for ='squaredOne1']")).size()==0)
			{
				System.out.println("Deleting contacts through Delete button in Volunteer History Successful.Testcase Pass");
			}
		 else
		
		 {
			 System.out.println("Deleting contacts through Delete button in Volunteer History unsuccessful.Testcase fail");
		 }
		 
		 
	 }
	 
	 public void CancelDelete() throws InterruptedException
	 {
	 firstcheckbox.click();
	 Thread.sleep(2000);
	 Delete1.click();
	 Thread.sleep(2000); 
	 Cancelbuttonpopup1stcheckbox.click();
	 Thread.sleep(3000);
	 System.out.println("Click Delete from popup overlay .Next click Cancel button on the Deletepop  does not delete the contact.TestCase Pass");
	 
}
	 
	 public void CancelDelete1() throws InterruptedException
	 {
		 FirstContactlink.click();
		 Thread.sleep(3000);
		 DeleteContact1stbutton.click();
		 Thread.sleep(2000);
		 Cancelbuttonpopup1stcheckboxvolunteerhistory.click();
		 
	 }
	 
	 public void DeleteAllContacts() throws InterruptedException
	 {
		 AllContactslink.click();
		 Thread.sleep(2000);
		 SelectAllCheckbox.click();
		 Thread.sleep(2000);
		 DeleteAll.click();
		 Thread.sleep(2000);
		 DeleteAllbutton.click();
		 Thread.sleep(3000);
		 if(driver.findElements(By.xpath("//label[@for ='squaredOne0']")).size()==0)
		 {
			 System.out.println("Deleting All Contacts Successfull.TestCase pass");
		 }
		 
		 else
		 {
			 System.out.println("Deleting All Contacts Unsuccessfull.TestCase failed");
		 }
		 }
			 
		 
		  
	 
	 public void deletecontactfromgroup() throws InterruptedException
	 {
		 FirstGroupleft.click();
		 Thread.sleep(2000);
		 firstcheckbox.click();
			Thread.sleep(2000);
			Delete1.click();
			Thread.sleep(2000);
			Deletebutton1stcheckbox.click();
			if (driver.findElements(By.xpath("//a[contains(text(),'Aaaa')]")).size()==0)
			{
				System.out.println("Deleting contacts through Delete button in Volunteer History Successful.Testcase Pass");
			}
		 else
		
		 {
			 System.out.println("Deleting contacts through Delete button in Volunteer History unsuccessful.Testcase fail");
		 }
		 
	 }
	 
	 public void deletemultiplecontactsfromgroup() throws InterruptedException
	 {
		 SecondGroupleft.click();
		 Thread.sleep(2000);
		 firstcheckbox.click();
		 secondcheckbox.click();
		 DeleteMultipleSelected.click();
		 Thread.sleep(3000);
		 DeleteMultiplebutton.click();
	 }
	 
	 public void clicksendmessage()
	 {
		 SendMessagebutton.click();
	 }
 
	 public void addsinglecontacttogroup() throws InterruptedException
	 {
		 
		 ManageGroup.click();
		 AddNewgroupfield.sendKeys("UniqueGroup7");
		Thread.sleep(4000);
		SaveGroup.click();
		Thread.sleep(2000);
		ManageGroup.click();
		Thread.sleep(6000);
		 AddNewgroupfield.sendKeys("UniqueGroup8");
		Thread.sleep(4000);
		SaveGroup.click();
		Thread.sleep(2000);
		
		if(driver.findElements(By.xpath("//a[contains(text(),UniqueGroup7)]")).size()>0)
		{
			System.out.println("Click on Manage- Enter a GroupName -Click Save -Group added successfully-Test case pass");
		}
		else
		{
			System.out.println("Click on Manage- Enter a GroupName -Click Save -Group added successfully-Test case pass");
		}
		
		Thread.sleep(2000);
		
		
		 firstcheckbox.click();
		 Thread.sleep(1000);
		 Add1togrouplink.click();
		 Thread.sleep(5000);
		 Addtogroup1stcheckbox.click();
		 Thread.sleep(1000);
		 AddtoGroupSavebutton.click();
		 Thread.sleep(4000);
		 FirstGroupleft.click();
		 Thread.sleep(2000);
		 if (driver.findElements(By.xpath("//label[@for ='squaredOne0']")).size()==1)
		 {
			 System.out.println("Single  contact added successfully to a custom group.Testcase pass");
		 }
		 
		 else
		 {
			 System.out.println("Single  contact not added successfully to a custom group.Testcase fail");
		 }
		 
		 AllContactslink.click();
	}
	 
	 
	 
	 public void addmultiplecontactstogroup() throws InterruptedException
	 {
		 firstcheckbox.click();
		 secondcheckbox.click();
		 Thread.sleep(1000);
		 Add2togrouplink.click();
		 //Addtogroup1stcheckbox.click();
		 //Thread.sleep(1000);
		 Addtogroup2ndcheckbox.click();
		 Thread.sleep(1000);
		 AddtoGroupSavebutton.click();
		 Thread.sleep(3000);
		
		 SecondGroupleft.click();
		 if (driver.findElements(By.xpath("//label[@for ='squaredOne1']")).size()==1)
		 {
			 System.out.println("Multiple  contact added successfully to a custom group.Testcase pass");
		 }
		 
		 else
		 {
			 System.out.println("Multiple  contact not added successfully to a custom group.Testcase fail");
		 }
		  AllContactslink.click();
		 }
	 
	 public void addallcontactstogroup() throws InterruptedException
	 {
		 SelectAllCheckbox.click();
		 Thread.sleep(1000);
		 AddAlltogrouplink.click();
		 Thread.sleep(1000);
		 Addtogroup3rdcheckbox.click();
		 Thread.sleep(1000);
		 AddtoGroupSavebutton.click();
		  }
	 
	 public void removesinglecontacttogroup() throws InterruptedException
	 {
		 ManageGroup.click();
		 AddNewgroupfield.sendKeys("UniqueGroup7");
		Thread.sleep(4000);
		SaveGroup.click();
		Thread.sleep(2000);
		firstcheckbox.click();
		 Thread.sleep(1000);
		 Add1togrouplink.click();
		 Thread.sleep(5000);
		 Addtogroup1stcheckbox.click();
		 Thread.sleep(1000);
		 AddtoGroupSavebutton.click();
		 
		 Thread.sleep(3000);
		 FirstGroupleft.click();
		 Thread.sleep(2000);
		 firstcheckbox.click();
		 Thread.sleep(2000);
		 RemovefromGroup1.click();
		 Thread.sleep(3000);
		 if(driver.findElements(By.xpath("//label[@for ='squaredOne0']")).size()==0)
		 {
          System.out.println("Single contact successfully removed from a  group.TestCase pass");
		 }
		 
		 else
		 {
			 System.out.println("Single contact not successfully removed from a  group.TestCase fail"); 
		 }
	 }
	 public void removemultiplecontact() throws InterruptedException
	 {
		 
		 
		 
		 SecondGroupleft.click();
		 Thread.sleep(3000);
		 firstcheckbox.click();
		 secondcheckbox.click();
		 Thread.sleep(2000);
		 RemovefromGroup2.click();
		 Thread.sleep(3000);
		 
		 if(driver.findElements(By.xpath("//label[@for ='squaredOne0']")).size()==0)
		 {
          System.out.println("Multiple contact successfully removed from a  group.TestCase pass");
		 }
		 
		 else
		 {
			 System.out.println("Multiple  contact not successfully removed from a  group.TestCase fail"); 
		 }
		 
		 
		}
	 
	 public void removeallcontact() throws InterruptedException
	 {
		 
		 
		 FirstGroupleft.click();
		 Thread.sleep(3000);
		 firstcheckbox.click();
		 secondcheckbox.click();
		 thirdcheckbox.click();
		 fourthcheckbox.click();
		 fifthcheckbox.click();
		 Thread.sleep(3000);
		 RemovefromGroup5.click();
		 
		 
		}
	 
	 
	 public void sorting()
	 {
		 GenericActions.singleselectdropdown( Sort, 1);
	 }
	 
	 public void helpoverlay() throws InterruptedException
	 {
		 HelpNext1.click();
		 Thread.sleep(2000);
		 HelpNext2.click();
		 Thread.sleep(2000);
		 HelpNext3.click();
		 Thread.sleep(4000);
		 HelpClose.click();
		 
	 }
	 

public void CurrentFairVolunteers() throws InterruptedException
{
	CurrentFairVolunteers.click();
	Thread.sleep(3000);
	firstcheckbox.click();
	Thread.sleep(5000);
	if(driver.findElement(By.xpath(".//*[@id='content']/div[2]/div/div/section[2]/div/div[2]/ul/li[1]/section/ul/li[5]")).isEnabled())
			{
		      System.out.println("Delete Option enabled for Current Fair Volunteers.Testcase failed");
			}
	else
	{
		System.out.println("Delete Option disabled for Current Fair Volunteers.Testcase pass");
	}
}

public void CurrentToolkitVolunteers() throws InterruptedException
{
	CurrentFairToolkitUsers.click();
	Thread.sleep(3000);
	firstcheckbox.click();
	Thread.sleep(5000);
	
	String s = driver.findElement(By.xpath(".//*[@id='content']/div[2]/div/div/section[2]/div/div[2]/ul/li/section/ul/li[5]")).getAttribute("class");
	
	System.out.println(driver.findElement(By.xpath(".//*[@id='content']/div[2]/div/div/section[2]/div/div[2]/ul/li/section/ul/li[5]")).getAttribute("class"));
	
	if(s=="disabled")
	{
		      System.out.println("Delete Option disabled for Current Toolkit Users.Testcase failed");
			}
	else
	{
		System.out.println("Delete Option enabled for Current ToolKitUsers.Testcase pass");
	}
}
  public void Export()
  {
	  Export.click();
  
  }  
  
  public void Importing() throws InterruptedException
  {
	  Import.click();
	  Thread.sleep(2000);
	  String url=driver.getCurrentUrl();
	 
	  if (url.equals("https://vms-qa.scholastic.com/#/contacts/import/502/370"))
		  
	  {
		  System.out.println("Click on Imports button navigates user to Import Contacts Page. Test Case Pass");
		  
		  
	  }
	  
	  else 
	  {
		  System.out.println("Click on Imports button does not navigate user to Import Contacts Page. Test Case Fail");
	  }

		  
	  
  
  }  
  
  public void Importclose()
  {
	  ImportPopupclosebutton.click();
	  System.out.println("Import of Contacts through csv file successful");
  }
  
}

  
  
  





