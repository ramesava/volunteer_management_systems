package com.scholastic.pagerepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VolunteersSignup {
	public WebDriver driver;


    @FindBy(xpath="//a[@class='ContactCount ng-binding']")
	private WebElement MyContactsLink;
    @FindBy(xpath="(//a[@class='email btn-custom'])[1]")
	private WebElement EmailInvite1stfair;
    @FindBy(xpath="(//a[@class='email btn-custom'])[2]")
   	private WebElement EmailInvite2ndfair;
    @FindBy(xpath="(//a[@class='email btn-custom'])[3]")
   	private WebElement EmailInvite3rdfair;
    @FindBy(xpath="(//a[@class='email btn-custom'])[4]")
   	private WebElement EmailInvite4thfair;
    @FindBy(xpath="(//button[contains(text(),'Edit Schedule')])[1]")
   	private WebElement EditSchedule1stfair;
    @FindBy(xpath="(//button[contains(text(),'Edit Schedule')])[2]")
   	private WebElement EditSchedule2ndfair;
    @FindBy(xpath="(//button[contains(text(),'Edit Schedule')])[3]")
   	private WebElement EditSchedule3rdfair;
    @FindBy(xpath="(//button[contains(text(),'Edit Schedule')])[4]")
   	private WebElement EditSchedule4thfair;
    @FindBy(xpath="(//button[@id='show'])[1]")
   	private WebElement ShowSchedule1stfair;
    @FindBy(xpath="(//button[@id='hide'])[1]")
   	private WebElement HideSchedule1stfair;
    
    @FindBy(xpath="(//button[@id='show'])[2]")
   	private WebElement ShowSchedule2ndfair;
    @FindBy(xpath="(//button[@id='hide'])[2]")
   	private WebElement HideSchedule2ndfair;
    @FindBy(xpath="(//button[@id='show'])[3]")
   	private WebElement ShowSchedule3rdfair;
    @FindBy(xpath="(//button[@id='show'])[4]")
   	private WebElement ShowSchedule4thfair;
    @FindBy(xpath="(//a[contains(text(),'Printable check list')])[1]")
   	private WebElement PrintableChecklist1stfair;
    @FindBy(xpath="(//a[contains(text(),'Printable check list')])[2]")
   	private WebElement PrintableChecklist2ndfair;
    @FindBy(xpath="(//a[contains(text(),'Printable check list')])[3]")
   	private WebElement PrintableChecklist3rdfair;
    @FindBy(xpath="(//a[contains(text(),'Printable check list')])[4]")
   	private WebElement PrintableChecklist4thfair;
    @FindBy(xpath="(//a[contains(text(),'Printable check list')])[11]")
   	private WebElement PrintableChecklist11thfair;
    @FindBy(xpath="(//button[contains(text(),'View Signup Sheet')])[1]")
   	private WebElement ViewSignupSheet1stfair;
    @FindBy(xpath="(//button[contains(text(),'View Signup Sheet')])[2]")
   	private WebElement ViewSignupSheet2ndfair;
    @FindBy(xpath="(//button[contains(text(),'View Signup Sheet')])[3]")
   	private WebElement ViewSignupSheet3rdfair;
    @FindBy(xpath="(//button[contains(text(),'View Signup Sheet')])[4]")
   	private WebElement ViewSignupSheet4thfair;
    @FindBy(xpath="(//button[@ng-click='createSheetRedirect(orgId, fair.fairId)'])[1]")
   	private WebElement CreateaSignup1stFair;
    @FindBy(xpath="(//button[@ng-click='createSheetRedirect(1000, fair.fairId)'])[2]")
   	private WebElement CreateaSignup2ndFair;
    @FindBy(xpath="(//button[@ng-click='createSheetRedirect(1000, fair.fairId)'])[3]")
   	private WebElement CreateaSignup3rdFair;
    @FindBy(xpath="(//button[@ng-click='createSheetRedirect(1000, fair.fairId)'])[4]")
   	private WebElement CreateaSignup4thFair;
    @FindBy(xpath="(//button[@ng-click='createSheetRedirect(1000, fair.fairId)'])[5]")
   	private WebElement CreateaSignup5thFair;
    @FindBy(xpath="(//button[@ng-click='createSheetRedirect(1000, fair.fairId)'])[11]")
   	private WebElement CreateaSignup11thFair;
    @FindBy(xpath="(//small[contains(text(),'Kollol Barua')])[1]")
   	private WebElement AssignedTask;
    @FindBy(xpath="html/body/div[2]/div/div/section[1]/div[1]/div[1]/div/div[3]/div/div/table[2]/tbody/tr[1]/td[2]/ul/li[25]")
   	private WebElement AssignedTask1;
    
    
    
    @FindBy(xpath="(//button[contains(text(),'EDIT')])[1]")
   	private WebElement EditTaskbutton;
    @FindBy(xpath="(//button[contains(text(),'CLOSE')])[1]")
   	private WebElement CloseTaskbutton;
    
    
    public VolunteersSignup(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
    
    
    public void clickmycontacts()
    {
    	MyContactsLink.click();
    }
    

public void clickeditschedule() throws InterruptedException
    {
    	
    	for(int i=1;i<=40;i++)
    	{
    		if(driver.findElement(By.xpath("(//button[contains(text(),'Edit Schedule')])["+i+"]")).isDisplayed()){
    			
    			
    			driver.findElement(By.xpath("(//button[contains(text(),'Edit Schedule')])["+i+"]")).click();
    			Thread.sleep(2000);
    			break;
    		}
    	}
    		
    		}

    
public void clickshowschedule() throws InterruptedException
{
	
	for(int i=1;i<=40;i++)
	{
		if(driver.findElement(By.xpath("(//button[@id='show'])["+i+"]")).isDisplayed()){
			
			
			driver.findElement(By.xpath("(//button[@id='show'])["+i+"]")).click();
			Thread.sleep(2000);
			break;
		}
	}
             
		Thread.sleep(2000);


    for(int i=1;i<=40;i++)
	{
		if(driver.findElement(By.xpath("(//button[@id='hide'])["+i+"]")).isDisplayed()){
			
			
			driver.findElement(By.xpath("(//button[@id='hide'])["+i+"]")).click();
			Thread.sleep(2000);
			break;
		}
	}


}
   

public void clickshowschedule1() throws InterruptedException
{
	
	for(int i=1;i<=40;i++)
	{
		if(driver.findElement(By.xpath("(//button[@id='show'])["+i+"]")).isDisplayed()){
			
			
			driver.findElement(By.xpath("(//button[@id='show'])["+i+"]")).click();
			Thread.sleep(2000);
			break;
		}
	}
}





public void clickemailinvite() throws InterruptedException
    {
    	
    	for(int i=1;i<=40;i++)
    	{
    		if(driver.findElement(By.xpath("(//a[@class='email btn-custom'])["+i+"]")).isDisplayed()){
    			
    			
    			driver.findElement(By.xpath("(//a[@class='email btn-custom'])["+i+"]")).click();
    			Thread.sleep(2000);
    			break;
    		}
    	}
    		
    		}
    
    

    public void clickcreatesignup() throws InterruptedException
    {
    	
    	
    	{
    		
    		CreateaSignup1stFair.click();
    		Thread.sleep(2000);
    		String url =driver.getCurrentUrl();
    		if (url.equals("https://vms-qa.scholastic.com/#/volunteers/schedule/503/372/new"))
    		System.out.println("Clicking On Create Sign up button,navigates user to Calender page.Testcase pass");
    		else
    		System.out.println("Clicking On Create Sign up button does not navigate user to Calender page");
    			
    	
    		
    	}
    	
    }
    
    
    public void clickcontinuecreation() throws InterruptedException
    {
    	
    	for(int i=1;i<=40;i++)
    	{
    		if(driver.findElement(By.xpath("(//button[@ng-click='loadSheetRedirect(orgId, fair.fairId,fair.signUpSheetId)'])["+i+"]")).isDisplayed()){
    			
    			
    			driver.findElement(By.xpath("(//button[@ng-click='loadSheetRedirect(orgId, fair.fairId,fair.signUpSheetId)'])["+i+"]")).click();
    			Thread.sleep(2000);
    			break;
    		}
    	}
    }
    
    public void clickPrintableCheckList() throws InterruptedException
    {
    	
    	for(int i=1;i<=40;i++)
    	{
    		if(driver.findElement(By.xpath("(//a[contains(text(),'Printable check list')])["+i+"]")).isDisplayed()){
    			
    			
    			driver.findElement(By.xpath("(//a[contains(text(),'Printable check list')])["+i+"]")).click();
    			Thread.sleep(2000);
    			break;
    		}
    	}
    }



    public void clickcontinuecreationindexwise()
    {
    	driver.findElement(By.xpath("(//button[contains(text(),'Edit Schedule')])[7]")).click();
    	
    }



    public void clickshowscheduleindexwise()
    {
    	driver.findElement(By.xpath("(//button[@id='show'])[1]")).click();
    	
    }

   public void editassignedtask() throws InterruptedException
   {
	   AssignedTask1.click();
	   Thread.sleep(3000);
	   EditTaskbutton.click();
	   
	   
   }

   public void cancelassignedtask()
   
   {
	   
   }
   
}







