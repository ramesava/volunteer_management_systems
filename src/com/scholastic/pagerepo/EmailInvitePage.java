package com.scholastic.pagerepo;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.genericutils.GenericActions;

public class EmailInvitePage {
	
	public WebDriver driver;
	
	@FindBy(xpath="//textarea[@name='email']")
	private WebElement Recipienttext;
	@FindBy(xpath="//textarea[@name='message']")
	private WebElement Messagetext;
	@FindBy(xpath="//input[@name='Send']")
	private WebElement SendEmail;
	@FindBy(xpath="//a[contains(text(),'Cancel')]")
	private WebElement CancelEmail;
	@FindBy(xpath="//button[contains(text(),'Okay')]")
	private WebElement Okay;
	
	 public EmailInvitePage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}


	 public void Sendemail() throws InterruptedException, IOException
		{
			Recipienttext.sendKeys("prasanjit.choudhury@relevancelab.com");
			 Messagetext.sendKeys("This is a test email");
			 Thread.sleep(1000);
			 SendEmail.click();
			 Thread.sleep(1000);
			Okay.click();
			Thread.sleep(1000);
			driver.navigate().to("https://vms-qa-web-1337328200.us-east-1.elb.amazonaws.com/#/volunteers/1000/500");
		}
		
		public void Cancelemail() throws InterruptedException, IOException
		{
			Recipienttext.sendKeys("prasanjit.choudhury@relevancelab.com");
			 Messagetext.sendKeys("This is a test email");
			 Thread.sleep(1000);
			 CancelEmail.click();
			 Thread.sleep(1000);
			 driver.navigate().to("https://vms-qa-web-1337328200.us-east-1.elb.amazonaws.com/#/volunteers/1000/500");
			 
			 
			 
	}

		
		public void SendEmailwithoutrecipentandmessage() throws InterruptedException, IOException
		{
			Recipienttext.sendKeys("");
			 Messagetext.sendKeys("");
			 Thread.sleep(1000);
			 SendEmail.click();
			 
			

	}
		public void CancelEmailwithoutrecipentandmessage() throws InterruptedException, IOException
		{
			Recipienttext.sendKeys("");
			 Messagetext.sendKeys("");
			 Thread.sleep(1000);
			 CancelEmail.click();
			 
		}

		
	}



