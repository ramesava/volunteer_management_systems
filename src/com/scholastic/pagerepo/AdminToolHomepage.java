package com.scholastic.pagerepo;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;
import com.scholastic.genericutils.GenericActions;

/**
 * Automated tests for Admin Tool Homepage.
 *  
 * @author Prasanjit Choudhury
 */
public class AdminToolHomepage {
	
	public WebDriver driver;
	@FindBy(partialLinkText="Book Fairs Role/Task Definition")
	private WebElement TaskDefinitionbutton;
	@FindBy(partialLinkText="Book Fairs Volunteer Message")
	private WebElement Messagebutton;
	

public AdminToolHomepage(WebDriver driver) {
	this.driver = driver;
	PageFactory.initElements(driver, this);
}

public void click() throws InterruptedException
{
	TaskDefinitionbutton.click();
	Thread.sleep(3000);
	String url=driver.getCurrentUrl();
	if (url.equals("https://vms-qa.scholastic.com/#/admin/roles"))
	{
		System.out.println("Clicking TaskDefinition button in Homepage, navigates the user to the TasksRolespage.Testcase passed");
	}
	
	else
	{
		System.out.println("Clicking TaskDefinition button in Homepage,  does not navigate the user to the TasksRolespage.Testcase failed");
	}
	
	driver.navigate().back();
	Thread.sleep(3000);
	Messagebutton.click();
	Thread.sleep(3000);
	String url1=driver.getCurrentUrl();
	if (url1.equals("https://vms-qa.scholastic.com/#/admin/message"))
	{
		System.out.println("Clicking VolunteerMessage button in Homepage, navigates the user to the VolunteerMessagepage.Testcase passed");
	}
	
	else
	{
		System.out.println("Clicking VolunteerMessage button in Homepage,  does not navigate the user to the VolunteerMessagepage.Testcase failed");
	}
	
	driver.navigate().refresh();
	driver.navigate().back();
	
	
	

}
public void clicks()
{
	TaskDefinitionbutton.click();
}
public void clicks1()
{
	Messagebutton.click();
}



public void textdisplay()
{
	WebElement Text = driver.findElement(By.xpath("//h2[contains(text(),'Volunteer Administration Tool')]"));
	boolean status =Text.isDisplayed();
	if (status==true)
	{
		System.out.println("Volunteer Administration Tool and Define and refine Roles/Tasks text displayed in the Admin Tool HomePage.Test case Passed");
	}
	
	else
	{
		System.out.println("Volunteer Administration Tool and Define and refine Roles/Tasks text  not  displayed in the Admin Tool HomePage.TestcaseFailed");
	}
}

}



