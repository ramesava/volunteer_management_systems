package com.scholastic.pagerepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SPSLoginPage {
	
	public WebDriver driver;
	@FindBy(xpath="//input[@name='email']")
	private WebElement Email;
	@FindBy(xpath="//input[@name='password']")
	private WebElement Password;
	@FindBy(xpath="//input[@type='submit']")
	private WebElement Submit;
	@FindBy(xpath="//a[contains(text(),'Register Today')]")
	private WebElement Register;
	@FindBy (xpath="//a[@ng-click='redirectToForgot()']")
	private WebElement Forgotpasswordlink;
	
	public SPSLoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void Spslogin() throws InterruptedException
	{
		Email.sendKeys("rpatwary@scholastic.com");
		Password.sendKeys("pass");
		Thread.sleep(3000);
		Submit.click();
		
	}
	
	public void Spslogin1() throws InterruptedException
	{
		Email.sendKeys("ma4564.wi78@gmail.com");
		Password.sendKeys("mawill");
		Thread.sleep(3000);
		Submit.click();
		
	}
	
	
	
	
	
	public void registeruser()
	{
		Register.click();
	}
	
	public void forgotpasswordlink()
	{
		Forgotpasswordlink.click();
	}

}
