package com.scholastic.pagerepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.scholastic.genericutils.GenericActions;

public class VolunteerSignupTaskDetails {
	
	public WebDriver driver;
	
    @FindBy(xpath="(//input[@name='tasks'])[1]")
	private WebElement FirstRole;
    @FindBy(xpath="(//input[@name='tasks'])[2]")
   	private WebElement SecondRole;
    @FindBy(xpath="(//input[@name='tasks'])[3]")
   	private WebElement ThirdRole;
    
    
    @FindBy(xpath="//input[@ng-model='task.title']")
	private WebElement TitleTextbox;
    @FindBy(xpath="//button[@ng-click='open($event)']")
	private WebElement Datepickerbutton;
    @FindBy(xpath="//select[@ng-model='task.start']")
	private WebElement StartTimeDropdown;
    @FindBy(xpath="//select[@ng-model='task.end']")
	private WebElement StartEndDropdown;
    @FindBy(xpath="//textarea[@ng-model='task.description']")
	private WebElement TaskDescription;
    @FindBy(xpath="//input[@ng-model='task.volName']")
   	private WebElement VolunteerName;
    @FindBy(xpath="//button[@ng-click='saveTask()']")
   	private WebElement Save;
    @FindBy(xpath="//button[@ng-click='saveDupTask()']")
   	private WebElement SaveandDuplicate;
    @FindBy(xpath="//a[contains(text(),'CANCEL')]")
   	private WebElement Cancel;
    @FindBy(xpath="//select[@ng-model='task.dups']")
   	private WebElement DuplicatesDropdown;
    @FindBy(xpath="//a[contains(text(),'Add new Contact')]")
   	private WebElement AddnewContact;
    @FindBy(xpath="//input[@name='firstName']")
   	private WebElement FirstName;
    @FindBy(xpath="//input[@name='lastName']")
   	private WebElement LastName;
    @FindBy(xpath="//input[@name='email']")
   	private WebElement Email;
    
    
    
    public VolunteerSignupTaskDetails (WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
    
    
    public void taskdetails() throws InterruptedException
    {
    	SecondRole.click();
    	System.out.println("Roles sucessfully selected from the Task Details Page.Test case pass ");
    	
    	TaskDescription.sendKeys("This is a test description");
    	
    	System.out.println("Roles description added in the Role Description box.Test case pass ");
    	//GenericActions.singleselectdropdown( StartTimeDropdown, 7);
    	//GenericActions.singleselectdropdown( StartEndDropdown, 8);
    	VolunteerName.sendKeys("Ajay Bora");
    	Thread.sleep(3000);
    	AddnewContact.click();
    	
    	
    	Thread.sleep(3000);
    	Email.sendKeys("Ajay.Bora@gmail.com");
    	
    	System.out.println("New Contacts successfully registered/added through the Roles and Tasks page");
    	Save.click();
    	Thread.sleep(3000);
    	String url=driver.getCurrentUrl();
    	Thread.sleep(2000);
    	if(url.equals("https://vms-qa.scholastic.com/#/volunteers/schedule/503/372"))
    	{
    		System.out.println("Clicking in Save in TaskDetails page navigates user back to Calendar page after adding roles and assigning volunteers.Test Case Pass");
    	}
    	
    	else
    		
    	{
    		System.out.println("Clicking in Save in TaskDetails page  does not  navigate user back to Calendar page after adding roles and assigning volunteers.Test Case Fail");
    	}
    	
    }
    
    public void taskdetails1() throws InterruptedException
    {
    	SecondRole.click();
    	//GenericActions.singleselectdropdown( StartTimeDropdown, 7);
    	//GenericActions.singleselectdropdown( StartEndDropdown, 8);
    	
    	TaskDescription.sendKeys("This is a test description");
    	VolunteerName.sendKeys("Ajay Bora");
    	Thread.sleep(3000);
    	GenericActions.singleselectdropdown(DuplicatesDropdown, 2);
    	SaveandDuplicate.click();
    	
    }
    
    
    public void taskdetails2() throws InterruptedException
    {
    	ThirdRole.click();
    	//GenericActions.singleselectdropdown( StartTimeDropdown, 7);
    	//GenericActions.singleselectdropdown( StartEndDropdown, 8);
    	
    	TaskDescription.sendKeys("This is a test description");
    	VolunteerName.sendKeys("NonExistent Volunteer ");
    	Thread.sleep(3000);
    	GenericActions.singleselectdropdown(DuplicatesDropdown, 3);
    	Save.click();
    	
    }
    
    public void edittaskdetails() throws InterruptedException
    {
    	FirstRole.click();
    	
    	TaskDescription.sendKeys("Edited Test description");
    	GenericActions.singleselectdropdown( StartTimeDropdown, 7);
    	GenericActions.singleselectdropdown( StartEndDropdown, 8);
    	VolunteerName.sendKeys("Halley Berry");
    	Thread.sleep(2000);
    	Save.click();
}

}