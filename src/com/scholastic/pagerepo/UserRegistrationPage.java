package com.scholastic.pagerepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserRegistrationPage {
	public WebDriver driver;
	@FindBy(xpath="//input[@name='firstName']")
	private WebElement FirstName;
	@FindBy(xpath="(//input[@name='lastName'])[1]")
	private WebElement LastName;
	@FindBy(xpath="//input[@name='email']")
	private WebElement Email;
	@FindBy(xpath="//input[@name='confirmEmail']")
	private WebElement ConfirmEmail;
	@FindBy(xpath="//input[@name='password']")
	private WebElement password;
	@FindBy(xpath="//input[@ng-model='user.confirmpassword']")
	private WebElement Confirmpassword;
	@FindBy(xpath="//input[@ng-model='user.terms']")
	private WebElement UserTermscheckbox;
	@FindBy(xpath="//input[@ng-model='user.privacyPolicy']")
	private WebElement UserPrivacyPolicyCheckbox;
	@FindBy(xpath="//button[@type='submit']")
	private WebElement Register;
	
	
	public UserRegistrationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

   public void registeruser() throws InterruptedException
   {
	   FirstName.sendKeys("Margareth");
	   LastName.sendKeys("Williams");
	   Email.sendKeys("ma4564.wi78@gmail.com");
	   ConfirmEmail.sendKeys("ma4564.wi78@gmail.com");
	   password.sendKeys("mawill");
	   Confirmpassword.sendKeys("mawill");
	   UserTermscheckbox.click();
	   UserPrivacyPolicyCheckbox.click();
	   Thread.sleep(2000);
	   Register.click();
	   driver.navigate().to("https://vms-dev-web-01-1942842308.us-east-1.elb.amazonaws.com/#/");
	   
   }

}

