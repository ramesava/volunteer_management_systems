package com.volunteermanagement.contacts;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import java.io.IOException;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.scholastic.genericutils.GenericActions;
import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.AddContactDetails;
import com.scholastic.pagerepo.AdminToolLoginpage;
import com.scholastic.pagerepo.MyContactsPage;
import com.scholastic.pagerepo.SPSLoginPage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest5;
public class EditContacts extends SuperTest5 {
	@Test
	
	public  void EditingContacts () throws IOException, InterruptedException
	{
		
		
		MyContactsPage c= new MyContactsPage(driver);
		
		
		AutomationLogger.getLogger().info("My Contacts Page displayed");
		//GenericActions.capturescreenshots(driver, "My Contacts Page Screenshot");
		Thread.sleep(3000);
		c.helpoverlay();
		c.EditContact();
		AddContactDetails a = new AddContactDetails(driver);
		AutomationLogger.getLogger().info("ContactDetails page  displayed");
		Thread.sleep(3000);
		//GenericActions.capturescreenshots(driver, "Contact details  Page screenshot");
		a.Editcontacts();
		Thread.sleep(3000);
		MyContactsPage d= new MyContactsPage(driver);
		Thread.sleep(2000);
		c.EditContact();
		AddContactDetails b = new AddContactDetails(driver);
		Thread.sleep(3000);
		b.EditcontactsCancel();
        
		
		driver.close();
    
			
}
}
