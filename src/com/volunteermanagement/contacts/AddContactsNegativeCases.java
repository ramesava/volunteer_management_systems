package com.volunteermanagement.contacts;

import org.testng.annotations.Test;
import org.testng.annotations.Test;

import java.io.IOException;

import org.testng.annotations.Test;

import com.scholastic.genericutils.GenericActions;
import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.AddContactDetails;
import com.scholastic.pagerepo.MyContactsPage;
import com.scholastic.pagerepo.SPSLoginPage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest5;

public class AddContactsNegativeCases extends SuperTest5{
	@Test
	public void NegativeTestcases() throws InterruptedException, IOException
	{
		
		
		MyContactsPage c= new MyContactsPage(driver);
		
		
		AutomationLogger.getLogger().info("My Contacts Page displayed");
		//GenericActions.capturescreenshots(driver, "My Contacts Page Screenshot");
		Thread.sleep(3000);
		c.helpoverlay();
		
		c.AddContact();
		AddContactDetails a = new AddContactDetails(driver);
		AutomationLogger.getLogger().info("ContactDetails page  displayed");
		Thread.sleep(3000);
		//GenericActions.capturescreenshots(driver, "Contact details  Page screenshot");
		a.Allfieldsblank1();
		Thread.sleep(3000);
		a.Allfieldsblank2();
		Thread.sleep(3000);
		a.Firstnamefieldblank1();
		Thread.sleep(3000);
		a.Firstnamefieldblank2();
		Thread.sleep(3000);
		a.Lastnamefieldblank1();
		Thread.sleep(3000);
		a.Lastnamefieldblank1();
		Thread.sleep(3000);
		
		driver.close();
//
	

}
}
