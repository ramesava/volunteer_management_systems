package com.volunteermanagement.contacts;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.SPSLoginPage;
import com.scholastic.supertest.SuperTest;

public class SpsLogin extends SuperTest {
	@Test
	public void spslogin() throws InterruptedException
	{
		SPSLoginPage l =new SPSLoginPage(driver);
		AutomationLogger.getLogger().info("SPS Login Page displayed");
		l.Spslogin();
		driver.close();
	}

}
