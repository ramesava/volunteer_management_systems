package com.volunteermanagement.contacts;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.MyContactsPage;
import com.scholastic.pagerepo.SPSLoginPage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest5;

public class CancelDeleteaction extends SuperTest5{
	@Test
	public void canceldelete() throws InterruptedException
	{
		
		
		MyContactsPage c= new MyContactsPage(driver);
		
		
		AutomationLogger.getLogger().info("My Contacts Page displayed");
		//GenericActions.capturescreenshots(driver, "My Contacts Page Screenshot");
		Thread.sleep(3000);
		c.helpoverlay();
		c.CancelDelete();
		Thread.sleep(3000);
		c.CancelDelete1();
		Thread.sleep(2000);
		if (driver.findElements(By.xpath("//a[contains(text(),'Aabey')]")).size()==0)
		{
			System.out.println("Clicking on Delete button through volunteer History and then cancelling delete operation deletes the contact.Test case fail");
		}
		else 
			
		{
			System.out.println("Clicking on Delete button through volunteer History and then cancelling delete operation does not  delete the contact.Test case pass");
		}
		driver.close();
}
	
}
