package com.volunteermanagement.contacts;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import java.io.IOException;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.scholastic.genericutils.GenericActions;
import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.AddContactDetails;
import com.scholastic.pagerepo.AdminToolLoginpage;
import com.scholastic.pagerepo.MyContactsPage;
import com.scholastic.pagerepo.SPSLoginPage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest3;
import com.scholastic.supertest.SuperTest5;

public class AddingContacts extends SuperTest5 {
	@Test
	
	public void Addcontacts() throws InterruptedException, IOException
	{
		
		
		
		
		MyContactsPage c= new MyContactsPage(driver);
		
	
		AutomationLogger.getLogger().info("My Contacts Page displayed");
		//GenericActions.capturescreenshots(driver, "My Contacts Page Screenshot");
		Thread.sleep(3000);
		c.helpoverlay();
		c.AddContact();
		AddContactDetails a = new AddContactDetails(driver);
		AutomationLogger.getLogger().info("ContactDetails page  displayed");
		Thread.sleep(3000);
		
		//GenericActions.capturescreenshots(driver, "Contact details  Page screenshot");
		a.addcontacts();
		Thread.sleep(3000);
		MyContactsPage c1= new MyContactsPage(driver);
		if (driver.findElements(By.xpath("//a[contains(text(),'Aabey')]")).size()==0)
		{
			System.out.println("Application issue. Contact is not added successfully");
		}
		//if (!driver.findElement(By.xpath("//a[contains(text(),'Aabey Aander')]")).isDisplayed())
		//{
			//System.out.println("Application issue. Contact is not added successfully");
		//}
		else
		{
			System.out.println(" Contact  added successfully on click of Save button");
		}
		
		c1.AddContact();
		a.addcontacts1();
		Thread.sleep(3000);
		String url=driver.getCurrentUrl();
		if(url.equals("https://vms-qa.scholastic.com/#/contacts/add/502/370"))
			
		{
			System.out.println("On clicking Save&Add,the user stays in the Add ContactDetails page .Testcase passed");
			}
			
			else
			{
			System.out.println("On clicking Save&Add,the user does not stay in the Add ContactDetails page .Testcase failed");	
			}
		
		
		a.addcontacts2();
		Thread.sleep(3000);
		
		if(driver.findElements(By.xpath("//a[contains(text(),'Aakey')]")).size()==0)
		{
		System.out.println("Contact  not Added  on click of Cancel Button.Testcase pass");
		}
		
		else
		{
		System.out.println("Contact  Added  on click of Cancel Button.Testcase fail");
		
		}
		
		if(driver.findElements(By.xpath("//a[contains(text(),'Anna')]")).size()==0)
		{
		System.out.println("Contact not added  on click of Save&Add Button.Testcase failed");
		}
		
		else
		{
		System.out.println("Contact  added successfully on click of Save&Add Button.Testcase pass");	
		}


driver.close();

		
		
	

}
	
}
