package com.volunteermanagement.contacts;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import com.scholastic.pagerepo.Forgotpasswordpage;
import com.scholastic.pagerepo.SPSLoginPage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest3;

public class Forgotpasswordnegative extends SuperTest3 {
	@Test
	public void forgotpsswrdneg() throws InterruptedException
	{
        SPSLoginPage l = new SPSLoginPage(driver);
		
		l.forgotpasswordlink();
		Forgotpasswordpage f =new Forgotpasswordpage(driver);
		Thread.sleep(3000);
		f.forgotpassword1();
		Thread.sleep(2000);
		f.forgotpassword2();
		Thread.sleep(2000);
		f.forgotpassword3();
		Thread.sleep(2000);
		f.forgotpassword5();
		driver.close();
	}

}
