package com.volunteermanagement.contacts;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import com.scholastic.logger.AutomationLogger;
import com.scholastic.pagerepo.MyContactsPage;
import com.scholastic.pagerepo.SPSLoginPage;
import com.scholastic.supertest.SuperTest;
import com.scholastic.supertest.SuperTest5;

public class Removefromgroups extends SuperTest5 {
 @Test
 public void removefromgroup() throws InterruptedException
 {
	
		
	   MyContactsPage c= new MyContactsPage(driver);
		
		
		AutomationLogger.getLogger().info("My Contacts Page displayed");
		//GenericActions.capturescreenshots(driver, "My Contacts Page Screenshot");
		Thread.sleep(3000);
		c.helpoverlay();
	    c.removesinglecontacttogroup();
	    Thread.sleep(3000);
	    c.removemultiplecontact();
	    //Thread.sleep(3000);
	    //c.removeallcontact();
	    driver.close();
 
 }
 
 
}
